#include "fname.h"
#include <string>
#include <algorithm>
#include <regex>

class LexemaExtractor
{
    std::string m_lexema;
    std::string::const_reverse_iterator m_lexema_end;
    int m_parenthesis_counter;
    int m_template_counters;
public:

    enum Result
    {
        CONTINUE,
        ERROR,
        EXTRACTED_NEXT,
        EXTRACTED_STOP
    };

    LexemaExtractor(std::string::const_reverse_iterator lexema_end, int parenthesis_counter)
        :m_lexema()
        ,m_lexema_end(lexema_end)
        ,m_parenthesis_counter(parenthesis_counter)
        ,m_template_counters(0)
    {
    }

    LexemaExtractor(std::string::const_reverse_iterator lexema_end)
        :m_lexema()
        ,m_lexema_end(lexema_end)
        ,m_parenthesis_counter(0)
        ,m_template_counters(0)
    {
    }

    LexemaExtractor::Result End(const std::string::const_reverse_iterator& it)
    {
        LexemaExtractor::Result pr = ERROR;
        if ( (m_template_counters == 0) && (m_parenthesis_counter == 0) )
        {
            m_lexema = std::string(m_lexema_end, it);
            std::reverse(m_lexema.begin(), m_lexema.end());
            pr = EXTRACTED_STOP;
        }
        return pr;
    }

    LexemaExtractor::Result Parse(std::string::const_reverse_iterator& it, std::string::const_reverse_iterator end)
    {
        static std::string operator_lexema("operator");
        static std::string stream_lt_lexema("operator<");
        static std::string stream_rt_lexema("operator>");
        LexemaExtractor::Result res = CONTINUE;        
        switch (*it)
        {
        case ')':
            m_parenthesis_counter++;
            break;
        case '(':
            m_parenthesis_counter--;
            if (m_parenthesis_counter < 0)
            {
                res = ERROR;
            }
            break;
        case '>':
            //Check operator
            if (Equal(operator_lexema.crbegin(), operator_lexema.crend(), it + 1, end))
            {
                it += operator_lexema.length();
            }
            else if (Equal(stream_rt_lexema.crbegin(), stream_rt_lexema.crend(), it + 1, end))
            {
                it += stream_rt_lexema.length();
            }
            else
            {
                m_template_counters++;
            }            
            break;
        case '<':
            if (Equal(operator_lexema.crbegin(), operator_lexema.crend(), it + 1, end))
            {
                it += operator_lexema.length();
            }
            else if (Equal(stream_lt_lexema.crbegin(), stream_lt_lexema.crend(), it + 1, end))
            {
                it += stream_lt_lexema.length();
            }
            else
            {
                m_template_counters--;
                if (m_template_counters < 0)
                {
                    res = ERROR;
                }
            }
            break;
        case ':':    
            it++;
            if ( (it == end) || (*it != ':') )
            {
                res = ERROR;
                break;
            }
        case ' ':
            if ( (m_template_counters == 0) && (m_parenthesis_counter == 0) )
            {
                std::string::const_reverse_iterator function_name_start = it;               
                if (*it == ':')
                {
                    --function_name_start;
                    res = EXTRACTED_NEXT;
                }
                else
                {
                    res = EXTRACTED_STOP;
                }
                m_lexema = std::string(m_lexema_end, function_name_start);
                std::reverse(m_lexema.begin(), m_lexema.end());
            }
            break;
        }
        return res;
    }
    
    std::string Lexema() const 
    { 
        return m_lexema; 
    }
private:

    bool Equal( std::string::const_reverse_iterator it1, 
                std::string::const_reverse_iterator end1, 
                std::string::const_reverse_iterator it2, 
                std::string::const_reverse_iterator end2)
    {
        bool res = true;
        while (it1 != end1)
        {
            if (it2 == end2)
            {
                res = false;
                break;
            }

            if (*it1 != *it2)
            {
                res = false;
                break;
            }
            ++it1;
            ++it2;
        }
        return res;
    }


};

bool xcov::FunctionNameParser::Parse(const std::string & function, const std::regex& classNamePattern, FunctionName & out)
{
    out.fn_class = "";
    out.fn_name = "";
    out.fn_namespace = "";
    out.fn_type = "";

    if (function.empty())
    {
        return false;
    }

    bool res = true;
    enum FunctionNameState
    {
        FUNCTION_TAIL,
        FUNCTION_NAME,
        FUNCTION_FIRST_NAMESPACE,
        FUNCTION_NAMESPACE,
        FUNCTION_RETURN_TYPE
    } state = FUNCTION_TAIL;

    //It is not possible to make a full parse of the function name.
    //The task is limited to extract class name and namespace
    //The class name is the last namespace that meet the regex class name
    //The trick is reverse parse, because we know that function is finisehd with something like ") [const]", then we count number of ")" and "(" and then we extract name
    LexemaExtractor le(function.crbegin());
    LexemaExtractor::Result pr = LexemaExtractor::CONTINUE;
    for ( auto it = function.crbegin(); res && (it != function.crend()); it++)
    {
        switch (state)
        {
        case FUNCTION_TAIL:
            if (*it == ')')
            {
                state = FUNCTION_NAME;
                le = LexemaExtractor(function.crbegin(), 1);
            }
            break;
        case FUNCTION_NAME:
            pr = le.Parse(it, function.crend());
            res = pr != LexemaExtractor::ERROR;
            if (pr == LexemaExtractor::EXTRACTED_NEXT)
            {
                state = FUNCTION_FIRST_NAMESPACE;
                out.fn_name = le.Lexema();
                le = LexemaExtractor(it + 1);
            }
            else if (pr == LexemaExtractor::EXTRACTED_STOP)
            {
                state = FUNCTION_RETURN_TYPE;
                out.fn_name = le.Lexema();
                le = LexemaExtractor(it + 1);
            }
            break;
        case FUNCTION_FIRST_NAMESPACE:
            pr = le.Parse(it, function.crend());
            res = pr != LexemaExtractor::ERROR;
            if ( (pr == LexemaExtractor::EXTRACTED_NEXT) || (pr == LexemaExtractor::EXTRACTED_STOP) )
            {
                state = (pr == LexemaExtractor::EXTRACTED_NEXT)? FUNCTION_NAMESPACE : FUNCTION_RETURN_TYPE;
                if (std::regex_match(le.Lexema(), classNamePattern))
                {
                    out.fn_class = le.Lexema();
                }
                else
                {
                    out.fn_namespace = le.Lexema() + "::" + out.fn_namespace;
                }                
                le = LexemaExtractor(it + 1);
            }
            break;
        case FUNCTION_NAMESPACE:
            pr = le.Parse(it, function.crend());
            res = pr != LexemaExtractor::ERROR;
            if (pr == LexemaExtractor::EXTRACTED_NEXT)
            {
                state = FUNCTION_NAMESPACE;
                out.fn_namespace = le.Lexema() + "::" + out.fn_namespace;
                le = LexemaExtractor(it + 1);
            }
            else if (pr == LexemaExtractor::EXTRACTED_STOP)
            {
                state = FUNCTION_RETURN_TYPE;
                out.fn_namespace = le.Lexema() + "::" + out.fn_namespace;
                le = LexemaExtractor(it + 1);
            }
            break;
        case FUNCTION_RETURN_TYPE:
            break;
        }
    }

    if (res)
    {
        switch (state)
        {
        case FUNCTION_TAIL:
            out.fn_name = function;
            break;
        case FUNCTION_NAME:
            pr = le.End(function.crend());
            if (pr == LexemaExtractor::EXTRACTED_NEXT || pr == LexemaExtractor::EXTRACTED_STOP)
            {
                out.fn_name = le.Lexema();
            }
            else
            {
                res = false;
            }
            break;
        case FUNCTION_FIRST_NAMESPACE:
            pr = le.End(function.crend());
            if (pr == LexemaExtractor::EXTRACTED_NEXT || pr == LexemaExtractor::EXTRACTED_STOP)
            {
                if (std::regex_match(le.Lexema(), classNamePattern))
                {
                    out.fn_class = le.Lexema();
                }
                else
                {
                    out.fn_namespace = le.Lexema() + "::" + out.fn_namespace;
                }
            }
            else
            {
                res = false;
            }
            break;
        case FUNCTION_NAMESPACE:
            pr = le.End(function.crend());
            if (pr == LexemaExtractor::EXTRACTED_NEXT || pr == LexemaExtractor::EXTRACTED_STOP)
            {
                out.fn_namespace = le.Lexema() + "::" + out.fn_namespace;
            }
            else
            {
                res = false;
            }
            break;
        case FUNCTION_RETURN_TYPE:
            pr = le.End(function.crend());
            if (pr == LexemaExtractor::EXTRACTED_NEXT || pr == LexemaExtractor::EXTRACTED_STOP)
            {
                out.fn_type = le.Lexema();
            }
            else
            {
                res = false;
            }

            break;
        }
    }

    return res;
}
