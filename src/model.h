#pragma once

#include <string>
#include <map>
#include <vector>
#include <memory>

/**
The model supported is gcov 8.x, but parser can convert old format:
version:gcc_version
file:source_file_name
function:start_line_number,end_line_number,execution_count,function_name
lcount:line number,execution_count,has_unexecuted_block
branch:line_number,branch_coverage_type

Where the branch_coverage_type is
   notexec (Branch not executed)
   taken (Branch executed and taken)
   nottaken (Branch executed, but not taken)
*/
namespace xcov
{
    class File;
    using FilePtr =     std::shared_ptr<File>;
    using FileLink =    std::weak_ptr<File>;
    using Files =       std::map<std::string, FilePtr >;

    class Class;
    using ClassPtr =    std::shared_ptr<Class>;
    using ClassLink =   std::weak_ptr<Class>;
    using Classes =     std::map<std::string, ClassPtr >;

    class Method;
    using MethodPtr =   std::shared_ptr<Method>;
    using MethodLink =  std::weak_ptr<Method>;
    using MethodsByName = std::map<std::string, MethodPtr>;
    using MethodsByLine = std::map<int, MethodPtr>;

    class Line;
    using LinePtr =     std::shared_ptr<Line>;
    using Lines =       std::map<int, LinePtr>;

    class Branch;
    using BranchPtr =   std::shared_ptr<Branch>;
    using Branches =    std::map<int, BranchPtr>;

    class File : public std::enable_shared_from_this<File>
    {
        std::string         m_name;
        MethodsByLine       m_methods;
        Classes             m_mentionedClasses;
        int                 m_associatedIndex;
    public:
        File(const std::string& name);
        File(const File& other) = default;
        File(File&& other);
        ~File() = default;
        MethodPtr AddMethod(const MethodPtr& method);

        MethodPtr GetMethod(int line) const;
        const MethodsByLine& GetMethods() const;
        const Classes& GetMentionedClasses() const;
        const std::string& GetName() const;
        int GetAssociatedIndex() const;
        void SetAssociatedIndex(int val);
    };

    class Line
    {
            int                 m_number;
            int                 m_executed;
            bool                m_hasUnexecutedBlock;
            MethodLink          m_method;        
        public:
            Line(int number, int executed, bool hasUnexecutedBlock, const MethodPtr& method);
            ~Line() = default;

            void                UpdateExecuted(int executed, bool hasUnexecutedBlock);
            int                 GetNumber() const;
            int                 GetExecuted() const;
            bool                HasUnexecutedBlock() const;
            MethodLink          GetMethod() const;                    
    };

    enum BranchExecutionType
    {
        NotExecuted,    //Branch not executed
        NotTaken,       //Branch executed, but not taken
        Taken,          //Branch executed and taken
    };
    using BranchVariants=std::vector<BranchExecutionType>;

    class Branch
    {
            int                 m_number;
            MethodLink          m_method;
            BranchVariants      m_variants;
        public:
            Branch(int number, const MethodPtr& method);
            Branch(const Branch& other) = default;
            Branch(Branch&& other);
            ~Branch() = default;
            void SetVariant(int variantNumber, BranchExecutionType variant);

            const BranchVariants& GetVariants() const;
            MethodLink GetMethod() const;

            int GetNumber() const;
    };

    class Method : public std::enable_shared_from_this<Method>
    {
            ClassLink           m_class;
            std::string         m_name;
            int                 m_startLine;
            int                 m_endLine;
            int                 m_executed;        
            FileLink            m_file;
            Lines               m_lines;
            Branches            m_branches;
            int                 m_associatedIndex;
        public:
            Method(const FilePtr& file, const ClassPtr& cls, const std::string& name, int start, int stop, int executed);
            Method(const Method& other) = default;
            Method(Method&& other);
            ~Method() = default;

            void UpdateExecuted(int executed);
            void SetEndLine(int stop);

            LinePtr AddLine(int number, int executed, bool hasUnexecutedBlock);
            BranchPtr AddBranch(int number, int variant_number, BranchExecutionType variant);

            LinePtr GetLine(int number) const;
            BranchPtr GetBranch(int number) const;

            const Lines& GetLines() const;
            const Branches& GetBranches() const;

            const std::string&  GetName() const;
            int                 GetStartLine() const;
            int                 GetEndLine() const;
            int                 GetExecuted() const;        
            const FileLink&      GetFile() const;  
            const ClassLink&     GetClass() const;
            int GetAssociatedIndex() const;
            void SetAssociatedIndex(int val);
    };

    class Class : public std::enable_shared_from_this<Class>
    {
            std::string         m_name;
            MethodsByName       m_methods;
            int                 m_associatedIndex;
        public:
            Class(const std::string& name);
            Class(const Class& other) = default;
            Class(Class&& other);
            ~Class() = default;
            MethodPtr AddMethod(const FilePtr& file, const std::string& name, int start, int stop, int executed);
            MethodPtr GetMethod(const std::string& name) const;
            const MethodsByName& GetMethods() const;
            const std::string& GetName() const;
            int GetAssociatedIndex() const;
            void SetAssociatedIndex(int val);
    };

    class Project
    {
        std::string     m_name;
        Files           m_files;
        Classes         m_classes;
    public:
        Project(const std::string& name);
        ~Project() = default;
        ClassPtr AddClass(const std::string& name);
        FilePtr AddFile(const std::string& name);

        ClassPtr GetClass(const std::string& name) const;
        FilePtr GetFile(const std::string& name) const;

        const Files& GetFiles() const;
        const Classes& GetClasses() const;

        const std::string& GetName() const; 
    };



}