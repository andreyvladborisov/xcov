#pragma once

#include <string>
#include <regex>

namespace xcov
{
    struct FunctionName
    {
        std::string fn_namespace;
        std::string fn_class;
        std::string fn_name;
        std::string fn_type;
    };

    class FunctionNameParser
    {
        FunctionNameParser() = delete;
    public:
        static bool Parse(const std::string& function, const std::regex& classNamePattern, FunctionName& out);
    };
}

