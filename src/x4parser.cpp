#include "x4parser.h"
#include <regex>
#include "fname.h"


namespace xcov
{
    static const std::string FILE_RGX = R"(^file:(.+)$)";
    static const std::string FUNCTION_RGX = R"(^function:\W*(\d+),\W*(\d+),\s*(.+)$)";
    static const std::string LINE_RGX = R"(^lcount:\W*(\d+),\W*(\d+)$)";
    static const std::string BRANCH_RGX = R"(^branch:\W*(\d+),\W*(taken|nottaken|notexec)$)";
    
    X4Parser::X4Parser(Project & project, const std::string& rxFiles, const std::string& rxClasses)
        :m_project(project)
        ,m_rgxAcceptedPath(rxFiles)
        ,m_rgxFile(FILE_RGX)
        ,m_rgxFunction(FUNCTION_RGX)
        ,m_rgxLCount(LINE_RGX)
        ,m_rgxBranch(BRANCH_RGX)
        ,m_rgxClasses(rxClasses)
        ,m_currentFile()
        ,m_currentMethod()
        ,m_lastBranch()
        ,m_branchCounter(0)
    {
    }

    X4Parser::~X4Parser()
    {
    }

    bool X4Parser::ParseString(const std::string & str)
    {
        bool res = false;
        std::smatch matches;
        if (std::regex_search(str, matches, m_rgxFile))
        {
            res = parseFile(matches);
        }
        else if (std::regex_search(str, matches, m_rgxFunction))
        {
            res = parseFunction(matches);
        }
        else if (std::regex_search(str, matches, m_rgxLCount))
        {
            res = parseLine(matches);
        }
        else if (std::regex_search(str, matches, m_rgxBranch))
        {
            res = parseBranch(matches);
        }
        else
        {
            res = false;
        }
        return res;
    }

    bool X4Parser::parseFile(const std::smatch& mt)
    {
        m_currentFile.reset();
        m_currentMethod.reset();
        m_lastBranch.reset();
        m_branchCounter = 0;
        
        if ( mt.size() == 2)
        {
            std::string file = mt[1].str();
            if (std::regex_search(file, m_rgxAcceptedPath))
            {
                m_currentFile = m_project.AddFile(file);
            }
        }
        return true;
    }

    bool X4Parser::parseFunction(const std::smatch& mt)
    {
        bool res = true;
        if (m_currentFile)
        {
            if (mt.size() == 4)
            {
                int line = std::stoi(mt[1].str());
                int executed = std::stoi(mt[2].str());
                std::string function = mt[3].str();
                FunctionName fn;
                res = FunctionNameParser::Parse(function, m_rgxClasses, fn);
                if (fn.fn_class.empty())
                {
                    std::string fileName = m_currentFile->GetName();
                    std::smatch matches;
                    if (std::regex_search(fileName, matches, m_rgxFileName))
                    {
                        fileName = matches[1].str();
                    }
                    fn.fn_class = fileName + ".GLOBAL";
                }
                if (res)
                {
                    ClassPtr cl = m_project.AddClass(fn.fn_namespace + fn.fn_class);
                    MethodPtr m = cl->AddMethod(m_currentFile, function, line, line, executed);
                    m_currentFile->AddMethod(m);
                }
            }
            else
            {
                res = false;
            }            
        }

        return res;
    }

    bool X4Parser::parseLine(const std::smatch& mt)
    {
        bool res = true;
        if (m_currentFile)
        {
            if (mt.size() == 3)
            {
                int line = std::stoi(mt[1].str());
                int executed = std::stoi(mt[2].str());

                MethodPtr method = m_currentFile->GetMethod(line);
                if (method)
                {
                    m_currentMethod = method;
                }
                else
                {
                    if (m_currentMethod)
                    {
                        m_currentMethod->SetEndLine(line);
                    }
                }

                if (m_currentMethod)
                {
                    m_currentMethod->AddLine(line, executed, false);
                }
                else
                {
                    res = true; //The function is not found - static object
                }
            }
            else
            {
                res = false;
            }            
        }
        return res;
    }

    bool X4Parser::parseBranch(const std::smatch& mt)
    {
        bool res = true;
        if (m_currentFile)
        {
            if (mt.size() == 3)
            {
                if (m_currentMethod)
                {
                    int line = std::stoi(mt[1].str());
                    std::string executed = mt[2].str();
                    BranchExecutionType extype;
                    if (executed == "taken")
                    {
                        extype = Taken;
                    }
                    else if (executed == "nottaken")
                    {
                        extype = NotTaken;
                    }
                    else
                    {
                        extype = NotExecuted;
                    }

                    if (m_lastBranch && (m_lastBranch->GetNumber() == line))
                    {
                        m_branchCounter++;
                    }
                    else
                    {
                        m_branchCounter = 0;
                    }
                    m_lastBranch = m_currentMethod->AddBranch(line, m_branchCounter, extype);
                }
                else
                {
                    res = true; //Just a static structure
                }
            }
            else
            {
                res = false;
            }
        }
        return res;
    }

}
