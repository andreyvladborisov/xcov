#include "model.h"

namespace xcov
{
    Project::Project(const std::string& name)
        :m_name(name)
        , m_files()
        , m_classes()
    {
    }

    ClassPtr Project::AddClass(const std::string& name)
    {
        ClassPtr ptr;
        Classes::const_iterator it = m_classes.find(name);
        if (it == m_classes.end())
        {
            ptr = std::make_shared<Class>(name);
            m_classes[name] = ptr;
        }
        else
        {
            ptr = it->second;
        }

        return ptr;
    }

    FilePtr Project::AddFile(const std::string& name)
    {
        FilePtr ptr;
        Files::const_iterator it = m_files.find(name);
        if (it == m_files.end())
        {
            ptr = std::make_shared<File>(name);
            m_files[name] = ptr;
        }
        else
        {
            ptr = it->second;
        }

        return ptr;
    }

    ClassPtr Project::GetClass(const std::string& name) const
    {
        ClassPtr ptr;
        Classes::const_iterator it = m_classes.find(name);
        if (it != m_classes.end())
        {
            ptr = it->second;
        }
        return ptr;
    }

    FilePtr Project::GetFile(const std::string& name) const
    {
        FilePtr ptr;
        Files::const_iterator it = m_files.find(name);
        if (it != m_files.end())
        {
            ptr = it->second;
        }
        return ptr;
    }

    const Files& Project::GetFiles() const
    {
        return m_files;
    }

    const Classes& Project::GetClasses() const
    {
        return m_classes;
    }

    const std::string& Project::GetName() const
    {
        return m_name;
    }

    Class::Class(const std::string& name)
        :std::enable_shared_from_this<Class>()
        , m_name(name)
        , m_methods()
        , m_associatedIndex(-1)
    {}


    Class::Class(Class&& other)
        :std::enable_shared_from_this<Class>(std::move(other))
        , m_name(std::move(other.m_name))
        , m_methods(std::move(other.m_methods))
        , m_associatedIndex(other.m_associatedIndex)
    {}

    MethodPtr Class::AddMethod(const FilePtr& file, const std::string& name, int start, int stop, int executed)
    {
        MethodPtr ptr;
        MethodsByName::const_iterator it = m_methods.find(name);
        if (it == m_methods.end())
        {
            ptr = std::make_shared<Method>(file, shared_from_this(), name, start, stop, executed);
            m_methods[name] = ptr;
        }
        else
        {
            ptr = it->second;
            ptr->UpdateExecuted(executed);
        }

        return ptr;
    }


    MethodPtr Class::GetMethod(const std::string& name) const
    {
        MethodPtr ptr;
        MethodsByName::const_iterator it = m_methods.find(name);
        if (it != m_methods.end())
        {
            ptr = it->second;
        }
        return ptr;
    }

    const MethodsByName& Class::GetMethods() const
    {
        return m_methods;
    }

    const std::string& Class::GetName() const
    {
        return m_name;
    }


    void Class::SetAssociatedIndex(int val)
    {
        m_associatedIndex = val;
    }

    int Class::GetAssociatedIndex() const
    {
        return m_associatedIndex;
    }

    Method::Method(const FilePtr& file, const ClassPtr& cls, const std::string& name, int start, int stop, int executed)
        :std::enable_shared_from_this<Method>()
        , m_class(cls)
        , m_name(name)
        , m_startLine(start)
        , m_endLine(stop)
        , m_executed(executed)
        , m_file(file)
        , m_lines()
        , m_branches()
        , m_associatedIndex(-1)
    {
    }

    Method::Method(Method&& other)
        :std::enable_shared_from_this<Method>(std::move(other))
        , m_class(std::move(other.m_class))
        , m_name(std::move(other.m_name))
        , m_startLine(other.m_startLine)
        , m_endLine(other.m_endLine)
        , m_executed(other.m_executed)
        , m_file(std::move(other.m_file))
        , m_lines(std::move(other.m_lines))
        , m_branches(std::move(other.m_branches))
        , m_associatedIndex(other.m_associatedIndex)
    {
    }

    void Method::UpdateExecuted(int executed)
    {
        m_executed += executed;
    }

    void Method::SetEndLine(int stop)
    {
        m_endLine = stop;
    }

    LinePtr Method::AddLine(int number, int executed, bool hasUnexecutedBlock)
    {
        LinePtr ptr;
        Lines::const_iterator it = m_lines.find(number);
        if (it == m_lines.end())
        {
            ptr = std::make_shared<Line>(number, executed, hasUnexecutedBlock, shared_from_this());
            m_lines[number] = ptr;
        }
        else
        {
            ptr = it->second;
            ptr->UpdateExecuted(executed, hasUnexecutedBlock);
        }

        return ptr;
    }

    BranchPtr Method::AddBranch(int number, int variantNumber, BranchExecutionType variant)
    {
        BranchPtr ptr;
        Branches::const_iterator it = m_branches.find(number);
        if (it == m_branches.end())
        {
            ptr = std::make_shared<Branch>(number, shared_from_this());
            m_branches[number] = ptr;
        }
        else
        {
            ptr = it->second;
        }

        if (ptr)
        {
            ptr->SetVariant(variantNumber, variant);
        }

        return ptr;
    }

    LinePtr Method::GetLine(int number) const
    {
        LinePtr ptr;
        Lines::const_iterator it = m_lines.find(number);
        if (it != m_lines.end())
        {
            ptr = it->second;
        }
        return ptr;
    }

    BranchPtr Method::GetBranch(int number) const
    {
        BranchPtr ptr;
        Branches::const_iterator it = m_branches.find(number);
        if (it != m_branches.end())
        {
            ptr = it->second;
        }
        return ptr;
    }

    const Lines& Method::GetLines() const
    {
        return m_lines;
    }

    const Branches& Method::GetBranches() const
    {
        return m_branches;
    }

    const std::string&  Method::GetName() const
    {
        return m_name;
    }

    int Method::GetStartLine() const
    {
        return m_startLine;
    }

    int Method::GetEndLine() const
    {
        return m_endLine;
    }

    int Method::GetExecuted() const
    {
        return m_executed;
    }

    const FileLink& Method::GetFile() const
    {
        return m_file;
    }

    const ClassLink & Method::GetClass() const
    {
        return m_class;
    }

    int Method::GetAssociatedIndex() const
    {
        return m_associatedIndex;
    }

    void Method::SetAssociatedIndex(int val)
    {
        m_associatedIndex = val;
    }

    Branch::Branch(int number, const MethodPtr & method)
        :m_number(number)
        ,m_method(method)
        ,m_variants()
    {
    }

    Branch::Branch(Branch && other)
        :m_number(other.m_number)
        ,m_method(std::move(other.m_method))
        ,m_variants(std::move(other.m_variants))
    {
    }

    void Branch::SetVariant(int variantNumber, BranchExecutionType variant)
    {
        while (m_variants.size() < static_cast<size_t>(variantNumber))
        {
            m_variants.push_back(NotExecuted);
        }

        if (static_cast<size_t>(variantNumber) < m_variants.size())
        {
            m_variants[variantNumber] = (variant > m_variants[variantNumber]) ? variant : m_variants[variantNumber];
        }
        else
        {
            m_variants.push_back(variant);
        }
    }

    const BranchVariants& Branch::GetVariants() const
    {
        return m_variants;
    }

    MethodLink Branch::GetMethod() const
    {
        return m_method;
    }

    int Branch::GetNumber() const
    {
        return m_number;
    }



    Line::Line(int number, int executed, bool hasUnexecutedBlock, const MethodPtr& method)
        :m_number(number)
        ,m_executed(executed)
        ,m_hasUnexecutedBlock(hasUnexecutedBlock)
        ,m_method(method)
    {
    }

    void Line::UpdateExecuted(int executed, bool hasUnexecutedBlock)
    {
        m_executed += executed;
        m_hasUnexecutedBlock = (hasUnexecutedBlock) ? hasUnexecutedBlock : m_hasUnexecutedBlock;
    }

    int Line::GetNumber() const
    {
        return m_number;
    }

    int Line::GetExecuted() const
    {
        return m_executed;
    }

    bool Line::HasUnexecutedBlock() const
    {
        return m_hasUnexecutedBlock;
    }

    MethodLink Line::GetMethod() const
    {
        return m_method;
    }

    File::File(const std::string & name)
        :std::enable_shared_from_this<File>()
        ,m_name(name)
        ,m_methods()
        ,m_mentionedClasses()
        ,m_associatedIndex(-1)
    {
    }

    File::File(File&& other)
        :std::enable_shared_from_this<File>(std::move(other))
        , m_name(std::move(other.m_name))
        , m_methods(std::move(other.m_methods))
        , m_mentionedClasses(std::move(other.m_mentionedClasses))
        , m_associatedIndex(other.m_associatedIndex)
    {
    }

    MethodPtr File::AddMethod(const MethodPtr & method)
    {
        if (method)
        {
            m_methods[method->GetStartLine()] = method;
            ClassPtr c = method->GetClass().lock();
            if (c)
            {
                m_mentionedClasses[c->GetName()] = c;
            }            
        }
        return method;
    }

    MethodPtr File::GetMethod(int line) const
    {
        MethodPtr ptr;
        MethodsByLine::const_iterator it = m_methods.find(line);
        if (it != m_methods.end())
        {
            ptr = it->second;
        }
        return ptr;
    }

    const MethodsByLine& File::GetMethods() const
    {
        return m_methods;
    }

    const Classes& File::GetMentionedClasses() const
    {
        return m_mentionedClasses;
    }

    const std::string & File::GetName() const
    {
        return m_name;
    }

    int File::GetAssociatedIndex() const
    {
        return m_associatedIndex;
    }

    void File::SetAssociatedIndex(int val)
    {
        m_associatedIndex = val;
    }

};