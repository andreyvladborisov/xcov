#pragma once
#include "igenerator.h"
#include <regex>

namespace xcov
{
    struct CloverMethodStatistic
    {
    public:
        //The cyclomatic complexity
        int complexity;
        //the number of contained conditionals(2 * number of branches)
        int conditionals;
        //the number of contained conditionals(2 * number of branches) with coverage
        int coveredconditionals;
        //the number of contained statements, conditionals and methods
        int elements;
        //the number of contained statements, conditionals and methods with coverage
        int coveredelements;
        //the number of contained statements
        int statements;
        //the number of contained statements with coverage
        int coveredstatements;
    };

    struct CloverClassStatistic : public CloverMethodStatistic 
    {
    public:
        //the number of contained methods
        int methods;
        //the number of contained methods with coverage
        int coveredmethods;
    };

    struct CloverFileStatistic : public CloverClassStatistic
    {
    public:
        //the total number of contained classes
        int classes;
        //the total number of lines of code    
        int loc;
        //the total number of non - comment lines of code
        int ncloc; 
    };

    struct CloverProjectStatistic : public CloverFileStatistic
    {
    public:
        //the total number of contained files
        int files;
    };

    using CloverStatisticForAllMethods = std::vector<CloverMethodStatistic>;
    using CloverStatisticForAllClasses = std::vector<CloverClassStatistic>;
    using CloverStatisticForAllFiles = std::vector<CloverFileStatistic>;

    struct CloverStatistic
    {
    public:
        CloverStatisticForAllMethods    methods;
        CloverStatisticForAllClasses    classes;
        CloverStatisticForAllFiles      files;
        CloverProjectStatistic          package;
    };

    class CloverGenerator : public IGenerator
    {
        std::regex m_rgxFileName { R"([\/]([^\^/]*$))" };
    public:
        CloverGenerator() = default;
        CloverGenerator(const CloverGenerator&) = delete;
        CloverGenerator& operator=(const CloverGenerator&) = delete;
        void Generate(const Project& project, std::ostream& st, std::chrono::seconds timestamp) const override;
        void GenerateStatitic(const Project& project, CloverStatistic& outStat) const;
    private:
        void GenerateStatisticForAllMethods(const Project& project, CloverStatistic& outStat) const;
        void GenerateStatisticForAllClasses(const Project& project, CloverStatistic& outStat) const;
        void GenerateStatisticForAllFiles(const Project& project, CloverStatistic& outStat) const;
        void GenerateStatisticForProject(const Project& project, CloverStatistic& outStat) const;
        void GenerateProject(const std::string& ident, const Project& project, std::ostream& st, const std::chrono::seconds timestamp, const CloverStatistic& stat) const;
        void GenerateFile(const std::string& ident, const FilePtr& file, std::ostream& st, const CloverStatistic& stat) const;
        void GenerateClass(const std::string& ident, const ClassPtr& cls, std::ostream& st, const CloverStatistic& stat) const;
        void GenerateLine(const std::string& ident, const LinePtr& second, std::ostream& st, const CloverStatistic& stat) const;
        void ReplaceAll(std::string &str, const std::string &pattern, const std::string &subst) const;

    };
}

