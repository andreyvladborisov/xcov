#pragma once
#include <chrono>
#include "model.h"

namespace xcov
{
    class IGenerator
    {
    public:
        virtual void Generate(const Project& project, std::ostream& st, std::chrono::seconds timestamp) const = 0;
    };
}

