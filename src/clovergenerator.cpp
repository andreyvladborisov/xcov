#include "clovergenerator.h"
#include <chrono>
#include <algorithm>
#include <set>

#define IDENT "    "

namespace xcov
{

    static std::ostream& operator<<(std::ostream& o, const CloverMethodStatistic& stat)
    {
        o << R"( complexity =")" << stat.complexity
            << R"(" elements=")" << stat.elements
            << R"(" coveredelements=")" << stat.coveredelements
            << R"(" conditionals=")" << stat.conditionals
            << R"(" coveredconditionals=")" << stat.coveredconditionals
            << R"(" statements=")" << stat.statements
            << R"(" coveredstatements=")" << stat.coveredstatements
            << R"(")";
        return o;
    }

    static std::ostream& operator<<(std::ostream& o, const CloverClassStatistic& stat)
    {
        o << static_cast<CloverMethodStatistic>(stat)
            << R"( coveredmethods=")" << stat.coveredmethods
            << R"(" methods=")" << stat.methods << R"(")";
        return o;
    }

    static std::ostream& operator<<(std::ostream& o, const CloverFileStatistic& stat)
    {
        o << static_cast<CloverClassStatistic>(stat)
            << R"( classes=")" << stat.classes
            << R"(" loc=")" << stat.loc
            << R"(" ncloc=")" << stat.ncloc << R"(")";
        return o;
    }

    static std::ostream& operator<<(std::ostream& o, const CloverProjectStatistic& stat)
    {
        o << static_cast<CloverFileStatistic>(stat)
            << R"( files=")" << stat.files << R"(")";
        return o;
    }


    void CloverGenerator::GenerateStatisticForAllMethods(const Project& project, CloverStatistic& outStat) const
    {
        size_t methods_count = 0;
        for (auto f : project.GetFiles())
        {
            methods_count += f.second->GetMethods().size();
        }
        outStat.methods.reserve(methods_count);

        for (auto f : project.GetFiles())
        {
            for (auto m : f.second->GetMethods())
            {                
                outStat.methods.emplace_back();
                CloverMethodStatistic& stat = outStat.methods.back();
                ::memset(&stat, 0, sizeof(CloverMethodStatistic));
                m.second->SetAssociatedIndex(outStat.methods.size() - 1);
                auto lines = m.second->GetLines();
                auto branches = m.second->GetBranches();
                stat.complexity = 1 + branches.size();
                for (auto b : branches)
                {
                    for (auto v : b.second->GetVariants())
                    {
                        ++stat.conditionals;
                        if (v != NotExecuted)
                        {
                            ++stat.coveredconditionals;
                        }
                    }                    
                }

                for (auto l : lines)
                {
                    if (l.second->GetExecuted() != 0)
                    {
                        stat.coveredelements++;
                    }
                    
                    if (l.second->GetNumber() == m.second->GetStartLine())
                    {
                        //The first line is considered to be a method
                        continue;
                    }

                    if (m.second->GetBranch(l.second->GetNumber()) == nullptr)
                    {
                        ++stat.statements;
                        if (l.second->GetExecuted() != 0)
                        {
                            ++stat.coveredstatements;
                        }
                    }
                }
                    
                stat.elements = m.second->GetLines().size();
            }
        }
    }

    void CloverGenerator::GenerateStatisticForAllClasses(const Project & project, CloverStatistic & outStat) const
    {
        outStat.classes.reserve(project.GetClasses().size());
        for (auto c : project.GetClasses())
        {
            outStat.classes.emplace_back();
            CloverClassStatistic& stat = outStat.classes.back();
            c.second->SetAssociatedIndex(outStat.classes.size() - 1);
            ::memset(&stat, 0, sizeof(CloverClassStatistic));
            stat.methods = c.second->GetMethods().size();
            for (auto m : c.second->GetMethods())
            {
                if (m.second->GetExecuted() != 0)
                {
                    ++stat.coveredmethods;
                }
                CloverMethodStatistic& mstat = outStat.methods[m.second->GetAssociatedIndex()];
                stat.complexity += mstat.complexity;
                stat.conditionals += mstat.conditionals;
                stat.coveredconditionals += mstat.coveredconditionals;
                stat.coveredelements += mstat.coveredelements;
                stat.coveredstatements += mstat.coveredstatements;
                stat.elements += mstat.elements;
                stat.statements += mstat.statements;
            }
        }
    }

    void CloverGenerator::GenerateStatisticForAllFiles(const Project & project, CloverStatistic & outStat) const
    {
        outStat.files.reserve(project.GetClasses().size());
        for (auto f : project.GetFiles())
        {
            outStat.files.emplace_back();
            CloverFileStatistic& stat = outStat.files.back();
            f.second->SetAssociatedIndex(outStat.files.size() - 1);
            ::memset(&stat, 0, sizeof(CloverFileStatistic));
            stat.methods = f.second->GetMethods().size();
            std::set<void*> classes;
            for (auto m : f.second->GetMethods())
            {
                if (m.second->GetExecuted() != 0)
                {
                    ++stat.coveredmethods;
                }
                CloverMethodStatistic& mstat = outStat.methods[m.second->GetAssociatedIndex()];
                stat.complexity += mstat.complexity;
                stat.conditionals += mstat.conditionals;
                stat.coveredconditionals += mstat.coveredconditionals;
                stat.coveredelements += mstat.coveredelements;
                stat.coveredstatements += mstat.coveredstatements;
                stat.elements += mstat.elements;
                stat.loc += m.second->GetLines().size();
                stat.ncloc += m.second->GetLines().size();
                stat.statements += mstat.statements;
                classes.insert(m.second->GetClass().lock().get());
            }
            stat.classes = classes.size();
        }
    }

    void CloverGenerator::GenerateStatisticForProject(const Project& project, CloverStatistic& outStat) const
    {
        CloverProjectStatistic& stat = outStat.package;
        ::memset(&stat, 0, sizeof(CloverProjectStatistic));       
        stat.files = project.GetFiles().size();
        for (auto f : project.GetFiles())
        {
            CloverFileStatistic& fstat = outStat.files[f.second->GetAssociatedIndex()];
            stat.classes += fstat.classes;
            stat.loc += fstat.loc;
            stat.ncloc += fstat.ncloc;
            stat.complexity += fstat.complexity;
            stat.conditionals += fstat.conditionals;
            stat.coveredconditionals += fstat.coveredconditionals;
            stat.coveredelements += fstat.coveredelements;
            stat.coveredmethods += fstat.coveredmethods;
            stat.coveredstatements += fstat.coveredstatements;
            stat.elements += fstat.elements;
            stat.methods += fstat.methods;
            stat.statements += fstat.statements;
        }
    }

    void CloverGenerator::Generate(const Project & project, std::ostream & st, std::chrono::seconds timestamp) const
    {
        CloverStatistic stat;
        GenerateStatitic(project, stat);
        st << R"(<?xml version="1.0" encoding="UTF-8"?>)" << std::endl;
        st << R"(<coverage generated=")" << timestamp.count() << R"(" clover="4.1.2">)" << std::endl;
        GenerateProject(IDENT, project, st, timestamp, stat);
        st << "</coverage>";
    }

    void CloverGenerator::GenerateStatitic(const Project & project, CloverStatistic & outStat) const
    {
        GenerateStatisticForAllMethods(project, outStat);
        GenerateStatisticForAllClasses(project, outStat);
        GenerateStatisticForAllFiles(project, outStat);
        GenerateStatisticForProject(project, outStat);
    }

    void CloverGenerator::GenerateProject(const std::string& ident, const Project& project, std::ostream& st, std::chrono::seconds timestamp, const CloverStatistic& stat) const
    {
        st << ident << R"(<project name=")" << project.GetName() << R"(" timestamp=")" << timestamp.count() << R"(">)" << std::endl;
        st << ident << IDENT << R"(<metrics )" << stat.package << R"( packages="1" )" << R"(/>)" << std::endl;
        st << ident << IDENT << R"(<package name=")" << project.GetName() << R"(">)" << std::endl;
        std::string current_ident = ident + IDENT + IDENT;
        st << current_ident << R"(<metrics )" << stat.package << R"(/>)" << std::endl;
        for (auto f : project.GetFiles())
        {
            GenerateFile(current_ident, f.second, st, stat);
        }
        st << ident << IDENT << R"(</package>)" << std::endl;
        st << ident << "</project>" << std::endl;
    }

    void CloverGenerator::GenerateFile(const std::string& ident, const FilePtr& file, std::ostream& st, const CloverStatistic& stat) const
    {        
        if (file)
        {
            std::smatch matches;
            std::string fileName = "no name";
            if (std::regex_search(file->GetName(), matches, m_rgxFileName))
            {
                fileName = matches[1].str();
            }
            const CloverFileStatistic& fstat = stat.files[file->GetAssociatedIndex()];
            st << ident << R"(<file name=")" << fileName << R"(" path=")" << file->GetName() << R"(">)" << std::endl;
            st << ident << IDENT << R"(<metrics )" << fstat << R"(/>)" << std::endl;
            for (auto c : file->GetMentionedClasses())
            {
                GenerateClass(ident + IDENT, c.second, st, stat);
            }

            for (auto m : file->GetMethods())
            {
                for (auto l : m.second->GetLines())
                {
                    GenerateLine(ident + IDENT, l.second, st, stat);
                }
            }

            st << ident << R"(</file>)" << std::endl;
        }
    }

    void CloverGenerator::GenerateClass(const std::string& ident, const ClassPtr& cls, std::ostream& st, const CloverStatistic& stat) const
    {        
        if (cls)
        {
            std::string className = cls->GetName();
            ReplaceAll(className, "::", ".");
            ReplaceAll(className, "&", "&amp;");
            ReplaceAll(className, "<", "&lt;");
            ReplaceAll(className, ">", "&gt;");
            
            const CloverClassStatistic& cstat = stat.classes[cls->GetAssociatedIndex()];
            st << ident << R"(<class name=')" << className << R"('>)" << std::endl;
            st << ident << IDENT << R"(<metrics )" << cstat << R"(/>)" << std::endl;
            st << ident << R"(</class>)" << std::endl;
        }
    }

    void CloverGenerator::GenerateLine(const std::string& ident, const LinePtr& l, std::ostream& st, const CloverStatistic& stat) const
    {
        enum LineType
        {
            METHOD,
            STATEMENT,
            CONDITION
        } lineType;

        /*
        @num - the line number
        @type - the type of syntactic construct - one of method|stmt|cond
        @complexity - only applicable if @type == 'method'; the cyclomatic complexity of the construct
        @count - only applicable if @type == 'stmt' or 'method'; the number of times the construct was executed
        @truecount - only applicable if @type == 'cond'; the number of times the true branch was executed
        @falsecount - only applicable if @type == 'cond'; the number of times the false branch was executed
        @signature - only applicable if @type == 'method'; the signature of the method
        @testduration - only applicable if @type == 'method' and the method was identified as a test method; the duration of the test
        @testsuccess - only applicable if @type == 'method' and the method was identified as a test method; true if the test passed, false otherwise
        @visibility - only applicable if @type == 'method'
        */
        if (l)
        {
            MethodPtr m = l->GetMethod().lock();
            std::string type = "stmt";
            lineType = STATEMENT;
            int count = l->GetExecuted();
            int trueCount = 0;
            int falseCount = 0;

            if (m)
            {
                if (m->GetStartLine() == l->GetNumber())
                {
                    lineType = METHOD;
                    type = "method";
                }
                else if (m->GetBranch(l->GetNumber()))
                {
                    lineType = CONDITION;
                    type = "cond";
                }

                const CloverMethodStatistic& mstat = stat.methods[m->GetAssociatedIndex()];

                if (lineType == CONDITION)
                {
                    for (auto v : m->GetBranch(l->GetNumber())->GetVariants())
                    {
                        if (v == Taken)
                        {
                            trueCount++;
                        }
                        else
                        {
                            falseCount++;
                        }
                    }
                }

                st << ident << R"(<line)";
                st << R"( num=")" << l->GetNumber() << R"(")";
                st << R"( type=")" << type << R"(")";
                if (lineType == METHOD)
                {
                    st << R"( complexity=")" << mstat.complexity << R"(")";
                    count = m->GetExecuted();
                }

                if ((lineType == METHOD) || (lineType == STATEMENT))
                {
                    st << R"( count=")" << count << R"(")";
                }

                if (lineType == CONDITION)
                {
                    st << R"( truecount=")" << trueCount << R"(")";
                    st << R"( falsecount=")" << falseCount << R"(")";
                }

                if (lineType == METHOD)
                {
                    std::string methodName = m->GetName();
                    ReplaceAll(methodName, "&", "&amp;");
                    ReplaceAll(methodName, "<", "&lt;");
                    ReplaceAll(methodName, ">", "&gt;");
                    st << R"( signature=')" << methodName << R"(')";
                }

                st << R"(/>)" << std::endl;
            }
        }
    }

    void CloverGenerator::ReplaceAll(std::string &str, const std::string &pattern, const std::string &subst) const
    {
        size_t pos = str.find(pattern);
        while (pos != std::string::npos)
        {
            str.replace(pos, pattern.length(), subst);
            pos = str.find(pattern, pos + pattern.length() + 1);
        }
    }

}

