#pragma once
#include "model.h"
#include <regex>

namespace xcov
{
    class X4Parser
    {
        std::regex  m_rgxFileName { R"([\/]([^\^/]*$))" };
        Project&    m_project;
        std::regex  m_rgxAcceptedPath;
        std::regex  m_rgxFile;
        std::regex  m_rgxFunction;
        std::regex  m_rgxLCount;
        std::regex  m_rgxBranch;
        std::regex  m_rgxClasses;
        FilePtr     m_currentFile;
        MethodPtr   m_currentMethod;
        BranchPtr   m_lastBranch;
        int         m_branchCounter;
    public:
        X4Parser(Project& project, const std::string& rxFiles, const std::string& rxClasses);
        ~X4Parser();
        bool ParseString(const std::string& str);
    private:
        bool parseFile(const std::smatch& mt);
        bool parseFunction(const std::smatch& mt);
        bool parseLine(const std::smatch& mt);
        bool parseBranch(const std::smatch& mt);
    };
}

