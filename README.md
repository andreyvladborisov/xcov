## Purpose
This utitlity generates clover.xml report for C++ projects.
The report is pretty complete and includes classes and methods statistic.
The report is inteded to be loaded into Bamboo.
*Currently Bamboo takes only top level statistic. Why he doesn't get the full info is under investigation.

## Internal
The project is written with C++ 11.
It uses gcov to create coverage info. It works correctly with CMake projects.

To include coverage information add the follwoing flags to gcc:
-fprofile-arcs -ftest-coverage -fno-default-inline -fno-inline -fno-inline-small-functions -fno-elide-constructors

## Compile
To compile the project run make.

## Usage
To run the application use:
use xcov v1.0 in the following way:
./xcov directory name [options]
Possible options:

--**project-name "name"**           The name of the project.
                                Default: "" 

--**build-directory path**          The path to the folder where the tool will create temporary folder with files. 
                                Default: folder gcov.XXXXXX will be created in current location. 
                                The folder will be removed after program finished.
--**object-directory path**         The path to the folder with *.gcda files
                                Default: directory name

--**output-file path**              Path to the output file. 
                                Default: the clover.xml file will be created at the current folder.

--**gcov-toolset path**             Path to gcov utility in case not a system installed utility is required.
                                Default: "gcov"

--**verbose-mode true|false**        Provide additional output.
                                Default: false

--**path-to-include "regex"**       The regex expression for select files that participate in report. 
                                Example: --path-to-include="/Lib/Cpp" - takes all files that include that subpath.
                                Default: .*

--**class-name-pattern "regex"**    The regex expression to differentiate the class name from namespace name. 
                                It is applied to the first lexem separated from function name with "::".
                                Example: --class-name-pattern="^T\w*" for the class name like TException.
                                Default: "^[A-Z]\w+".
