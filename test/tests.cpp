#include "tests.h"
#include "model_test.h"
#include "fname_test.h"
#include "x4parser_test.h"
#include "clover_test.h"


int main()
{
    RUN_TEST(Test_Model_Project);
    RUN_TEST(Test_Model_Class);
    RUN_TEST(Test_Model_Method);
    RUN_TEST(Test_Model_Branch);
    RUN_TEST(Test_Model_Line);
    RUN_TEST(Test_Model_File);
    RUN_TEST(Test_FName_Happy);
    RUN_TEST(Test_FName_Error);
    RUN_TEST(Test_X4Parser_Happy);
    RUN_TEST(Test_X4Parser_Error);
    RUN_TEST(Test_Clover_Statistic);
    RUN_TEST(Test_Clover_Generator);

    return TEST_OK;
}