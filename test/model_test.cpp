#include "model_test.h"
#include "tests.h"

bool Test_Model_Project()
{
    START_TEST("add classes and files");
    {
        xcov::Project p("test");
        ASSERT_EQUAL("test", p.GetName());
        ASSERT_EQUAL(0, p.GetClasses().size());

        auto cls1 = p.AddClass("Class1");
        auto cls2 = p.AddClass("Class2");
        ASSERT_NOT_EQUAL(cls1, cls2);
        ASSERT_EQUAL(2, p.GetClasses().size());
        auto cls3 = p.AddClass("Class1");
        ASSERT_EQUAL(cls1, cls3);
        ASSERT_EQUAL(2, p.GetClasses().size());
        auto cls4 = p.GetClass("Class2");
        ASSERT_EQUAL(cls2, cls4);
        auto cls5 = p.GetClass("Class3");
        ASSERT_FALSE(cls5);

        auto f1 = p.AddFile("File1");
        auto f2 = p.AddFile("File2");
        ASSERT_NOT_EQUAL(f1, f2);
        ASSERT_EQUAL(2, p.GetFiles().size());
        auto f3 = p.AddFile("File1");
        ASSERT_EQUAL(f1, f3);
        ASSERT_EQUAL(2, p.GetFiles().size());
        auto f4 = p.GetFile("File2");
        ASSERT_EQUAL(f2, f4);
        auto f5 = p.GetClass("File3");
        ASSERT_FALSE(f5);

    }
    END_TEST;
}

bool Test_Model_Class()
{
    START_TEST("add methods, update execution count");
    {
        xcov::FilePtr f = std::make_shared<xcov::File>("File");
        xcov::ClassPtr cls = std::make_shared<xcov::Class>("Class");
        ASSERT_EQUAL("Class", cls->GetName());
        xcov::MethodPtr m1 = cls->AddMethod(f, "foo()", 13, 0, 2);
        ASSERT_TRUE(m1);
        xcov::MethodPtr m2 = cls->AddMethod(f, "bar()", 22, 0, 3);
        ASSERT_EQUAL(2, cls->GetMethods().size());
        xcov::MethodPtr m3 = cls->GetMethod("foo()");
        ASSERT_EQUAL(m1, m3);
        xcov::MethodPtr m4 = cls->GetMethod("foo1()");
        ASSERT_FALSE(m4);
        xcov::MethodPtr m5 = cls->AddMethod(f, "foo()", 13, 0, 5);
        ASSERT_EQUAL(m1, m5);
        ASSERT_EQUAL(7, m5->GetExecuted());
    }
    END_TEST;
}

bool Test_Model_Method()
{
    START_TEST("create method, update it, add line, add branch");
    {
        xcov::FilePtr f = std::make_shared<xcov::File>("File");
        xcov::ClassPtr c = std::make_shared<xcov::Class>("Class");
        xcov::MethodPtr m = std::make_shared<xcov::Method>(f, c, "foo()", 12, 0, 2);
        ASSERT_EQUAL("foo()", m->GetName());
        ASSERT_EQUAL(12, m->GetStartLine());
        ASSERT_EQUAL(0, m->GetEndLine());
        ASSERT_EQUAL(2, m->GetExecuted());
        ASSERT_EQUAL(f, m->GetFile().lock());
        ASSERT_EQUAL(c, m->GetClass().lock());

        m->SetEndLine(22);
        ASSERT_EQUAL(22, m->GetEndLine());
        m->UpdateExecuted(10);
        ASSERT_EQUAL(12, m->GetExecuted());

        xcov::LinePtr l1 = m->AddLine(14, 3, false);
        xcov::LinePtr l2 = m->AddLine(15, 1, false);
        ASSERT_EQUAL(2, m->GetLines().size());
        xcov::LinePtr l3 = m->GetLine(14);
        ASSERT_EQUAL(l1, l3);
        xcov::LinePtr l4 = m->GetLine(18);
        ASSERT_FALSE(l4);
        xcov::LinePtr l6 = m->AddLine(14, 5, false);
        ASSERT_EQUAL(l1, l6);
        ASSERT_EQUAL(8, l6->GetExecuted());

        xcov::BranchPtr b1 = m->AddBranch(14, 0, xcov::NotExecuted);
        xcov::BranchPtr b2 = m->AddBranch(15, 0, xcov::NotExecuted);
        ASSERT_EQUAL(2, m->GetBranches().size());
        xcov::BranchPtr b3 = m->GetBranch(14);
        ASSERT_EQUAL(b1, b3);
        xcov::BranchPtr b4 = m->AddBranch(14, 1, xcov::NotExecuted);
        ASSERT_EQUAL(2, m->GetBranches().size());
        ASSERT_EQUAL(b1, b4);
        ASSERT_EQUAL(2, b1->GetVariants().size());
        xcov::BranchPtr b5 = m->AddBranch(14, 1, xcov::Taken);
        ASSERT_EQUAL(b1, b5)
        ASSERT_EQUAL(2, b1->GetVariants().size());
        ASSERT_EQUAL(xcov::NotExecuted, b1->GetVariants().at(0));
        ASSERT_EQUAL(xcov::Taken, b1->GetVariants().at(1));
        xcov::BranchPtr b6 = m->GetBranch(18);
        ASSERT_FALSE(b6);
    }
    END_TEST;
}

bool Test_Model_Branch()
{
    START_TEST("create branch, add variant");
    {
        xcov::FilePtr f = std::make_shared<xcov::File>("File");
        xcov::ClassPtr c = std::make_shared<xcov::Class>("Class");
        xcov::MethodPtr m = std::make_shared<xcov::Method>(f, c, "foo()", 12, 0, 2);
        xcov::BranchPtr b = std::make_shared<xcov::Branch>(1, m);
        ASSERT_EQUAL(1, b->GetNumber());
        ASSERT_EQUAL(m, b->GetMethod().lock());
        b->SetVariant(0, xcov::NotExecuted);
        b->SetVariant(3, xcov::NotTaken);
        ASSERT_EQUAL(4, b->GetVariants().size());
        ASSERT_EQUAL(xcov::NotExecuted, b->GetVariants().at(0));
        ASSERT_EQUAL(xcov::NotExecuted, b->GetVariants().at(1));
        ASSERT_EQUAL(xcov::NotExecuted, b->GetVariants().at(2));
        ASSERT_EQUAL(xcov::NotTaken, b->GetVariants().at(3));
        b->SetVariant(3, xcov::Taken);
        ASSERT_EQUAL(4, b->GetVariants().size());
        ASSERT_EQUAL(xcov::Taken, b->GetVariants().at(3));
        b->SetVariant(3, xcov::NotTaken);
        ASSERT_EQUAL(4, b->GetVariants().size());
        ASSERT_EQUAL(xcov::Taken, b->GetVariants().at(3));
    }
    END_TEST;
}

bool Test_Model_Line()
{
    START_TEST("create line, update executed");
    {
        xcov::FilePtr f = std::make_shared<xcov::File>("File");
        xcov::ClassPtr c = std::make_shared<xcov::Class>("Class");
        xcov::MethodPtr m = std::make_shared<xcov::Method>(f, c, "foo()", 12, 0, 2);
        xcov::LinePtr l = std::make_shared<xcov::Line>(10, 12, true, m);
        ASSERT_EQUAL(10, l->GetNumber());
        ASSERT_EQUAL(12, l->GetExecuted());
        ASSERT_EQUAL(true, l->HasUnexecutedBlock());
        ASSERT_EQUAL(m, l->GetMethod().lock());
        l->UpdateExecuted(10, false);
        ASSERT_EQUAL(22, l->GetExecuted());
        ASSERT_EQUAL(true, l->HasUnexecutedBlock());
    }
    END_TEST;
}

bool Test_Model_File()
{
    START_TEST("");
    {
        xcov::FilePtr f = std::make_shared<xcov::File>("File");
        ASSERT_EQUAL("File", f->GetName());

        xcov::ClassPtr c = std::make_shared<xcov::Class>("Class");
        xcov::MethodPtr m1 = std::make_shared<xcov::Method>(f, c, "foo()", 12, 0, 2);

        xcov::MethodPtr m2 = f->AddMethod(m1);
        ASSERT_EQUAL(m1, m2);
        xcov::MethodPtr m3 = f->GetMethod(12);
        ASSERT_EQUAL(m1, m3);
        xcov::MethodPtr m4 = f->GetMethod(13);
        ASSERT_FALSE(m4);
        ASSERT_EQUAL(1, f->GetMethods().size());

        const xcov::Classes& css = f->GetMentionedClasses();
        ASSERT_EQUAL(1, css.size());
        xcov::Classes::const_iterator it = css.find("Class");
        if (it != css.end())
        {
            ASSERT_EQUAL(c, it->second);
        }
        else
        {
            FAIL_TEST("class not found");
        }        
    }
    END_TEST;
}
