#include "fname_test.h"
#include "tests.h"
#include "..\src\fname.h"
#include <regex>

bool Test_FName_Happy()
{
    START_TEST("parse happy path");
    {
        xcov::FunctionName fn;
        bool res;
        res = xcov::FunctionNameParser::Parse("foo1()", std::regex(""), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("", fn.fn_namespace);
        ASSERT_EQUAL("", fn.fn_class);
        ASSERT_EQUAL("foo1()", fn.fn_name);
        ASSERT_EQUAL("", fn.fn_type);

        res = xcov::FunctionNameParser::Parse("void foo2()", std::regex("^[A-Z]\\w+"), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("", fn.fn_namespace);
        ASSERT_EQUAL("", fn.fn_class);
        ASSERT_EQUAL("foo2()", fn.fn_name);
        ASSERT_EQUAL("void", fn.fn_type);
        
        res = xcov::FunctionNameParser::Parse("const char* foo3() const", std::regex("^[A-Z]\\w+"), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("", fn.fn_namespace);
        ASSERT_EQUAL("", fn.fn_class);
        ASSERT_EQUAL("foo3() const", fn.fn_name);
        ASSERT_EQUAL("const char*", fn.fn_type);

        res = xcov::FunctionNameParser::Parse("const std::vector<top::pop::TUri>& top::pop::Class::foo2(int a, int b)", std::regex("^[A-Z]\\w+"), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("top::pop::", fn.fn_namespace);
        ASSERT_EQUAL("Class", fn.fn_class);
        ASSERT_EQUAL("foo2(int a, int b)", fn.fn_name);
        ASSERT_EQUAL("const std::vector<top::pop::TUri>&", fn.fn_type);
        
        res = xcov::FunctionNameParser::Parse("top::pop::TUri::~TUri()", std::regex("^T\\w+"), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("top::pop::", fn.fn_namespace);
        ASSERT_EQUAL("TUri", fn.fn_class);
        ASSERT_EQUAL("~TUri()", fn.fn_name);
        ASSERT_EQUAL("", fn.fn_type);

        res = xcov::FunctionNameParser::Parse("top::pop::TUri::operator==(top::pop::TUri const&) const", std::regex("^T\\w+"), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("top::pop::", fn.fn_namespace);
        ASSERT_EQUAL("TUri", fn.fn_class);
        ASSERT_EQUAL("operator==(top::pop::TUri const&) const", fn.fn_name);
        ASSERT_EQUAL("", fn.fn_type);

        res = xcov::FunctionNameParser::Parse("void std::_Destroy<top::uti::TUri*, top::uti::TUri*>(top::uti::TUri*, top::uti::TUri*, std::allocator<top::uti::TUri*>&)", std::regex("^T\\w+"),fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("std::", fn.fn_namespace);
        ASSERT_EQUAL("", fn.fn_class);
        ASSERT_EQUAL("_Destroy<top::uti::TUri*, top::uti::TUri*>(top::uti::TUri*, top::uti::TUri*, std::allocator<top::uti::TUri*>&)", fn.fn_name);
        ASSERT_EQUAL("void", fn.fn_type);

        res = xcov::FunctionNameParser::Parse("void noclass::qsort(void *base, size_t nmemb, size_t size, int(*compar)(const void *, const void *))", std::regex("^[A-Z]\\w+"), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("noclass::", fn.fn_namespace);
        ASSERT_EQUAL("", fn.fn_class);
        ASSERT_EQUAL("qsort(void *base, size_t nmemb, size_t size, int(*compar)(const void *, const void *))", fn.fn_name);
        ASSERT_EQUAL("void", fn.fn_type);

        res = xcov::FunctionNameParser::Parse("hme::exceptions::TException::operator<(hme::exceptions::TException const&) const", std::regex("^T\\w+"), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("hme::exceptions::", fn.fn_namespace);
        ASSERT_EQUAL("TException", fn.fn_class);
        ASSERT_EQUAL("operator<(hme::exceptions::TException const&) const", fn.fn_name);
        ASSERT_EQUAL("", fn.fn_type);

        res = xcov::FunctionNameParser::Parse("hme::exceptions::TException::operator>=(hme::exceptions::TException const&) const", std::regex("^T\\w+"), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("hme::exceptions::", fn.fn_namespace);
        ASSERT_EQUAL("TException", fn.fn_class);
        ASSERT_EQUAL("operator>=(hme::exceptions::TException const&) const", fn.fn_name);
        ASSERT_EQUAL("", fn.fn_type);

        res = xcov::FunctionNameParser::Parse("hme::exceptions::TException::operator<<(const std::ostream& stm) const", std::regex("^T\\w+"), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("hme::exceptions::", fn.fn_namespace);
        ASSERT_EQUAL("TException", fn.fn_class);
        ASSERT_EQUAL("operator<<(const std::ostream& stm) const", fn.fn_name);
        ASSERT_EQUAL("", fn.fn_type);

        res = xcov::FunctionNameParser::Parse("hme::exceptions::TException::operator>>(const std::ostream& stm) const", std::regex("^T\\w+"), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("hme::exceptions::", fn.fn_namespace);
        ASSERT_EQUAL("TException", fn.fn_class);
        ASSERT_EQUAL("operator>>(const std::ostream& stm) const", fn.fn_name);
        ASSERT_EQUAL("", fn.fn_type);

        res = xcov::FunctionNameParser::Parse("int(*TClass::compare_function(void))(int a)", std::regex("^[A-Z]\\w+"), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("", fn.fn_namespace);
        ASSERT_EQUAL("", fn.fn_class);
        ASSERT_EQUAL("int(*TClass::compare_function(void))(int a)", fn.fn_name);

        res = xcov::FunctionNameParser::Parse("_GLOBAL__sub_I_TTCPStream.cpp", std::regex("^[A-Z]\\w+"), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("", fn.fn_namespace);
        ASSERT_EQUAL("", fn.fn_class);
        ASSERT_EQUAL("_GLOBAL__sub_I_TTCPStream.cpp", fn.fn_name);

        res = xcov::FunctionNameParser::Parse("(anonymous namespace)::TTLSHandle::GetDecrypted(unsigned char*, unsigned int, unsigned int, t_tls_result&)", std::regex("^[A-Z]\\w+"), fn);
        ASSERT_TRUE(res);
        ASSERT_EQUAL("(anonymous namespace)::", fn.fn_namespace);
        ASSERT_EQUAL("TTLSHandle", fn.fn_class);
        ASSERT_EQUAL("GetDecrypted(unsigned char*, unsigned int, unsigned int, t_tls_result&)", fn.fn_name);

    }
    END_TEST;
}

bool Test_FName_Error()
{
    START_TEST("parse error");
    {
        xcov::FunctionName fn;
        bool res;

        res = xcov::FunctionNameParser::Parse("void foo2)", std::regex("^[A-Z]\\w+"), fn);
        ASSERT_FALSE(res);

        res = xcov::FunctionNameParser::Parse("const char* foo3(() const", std::regex("^[A-Z]\\w+"), fn);
        ASSERT_FALSE(res);

        res = xcov::FunctionNameParser::Parse("const std::vector top::pop::TUri>&top.pop.Class.foo2(int a, int b)", std::regex("^[A-Z]\\w+"), fn);
        ASSERT_FALSE(res);

        res = xcov::FunctionNameParser::Parse("top::pop:::TUri::~TUri()", std::regex("^T\\w+"), fn);
        ASSERT_FALSE(res);

        res = xcov::FunctionNameParser::Parse("void std::_Destroy<top::uti::TUri*<, top::uti::TUri*>(top::uti::TUri*, top::uti::TUri*, std::allocator<top::uti::TUri*>&)", std::regex("^T\\w+"), fn);
        ASSERT_FALSE(res);
    }
    END_TEST;
}
