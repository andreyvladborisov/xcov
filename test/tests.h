#pragma once
#include <iostream>

#define TEST_OK 0
#define TEST_FAILED -1
#ifdef __linux__
#define COLOR_RED "\033[1;31m"
#define COLOR_GREEN "\033[1;32m"
#define COLOR_BACK "\033[0m"
#else
#define COLOR_RED " > !!!!!!!!!"
#define COLOR_GREEN " > ++++++++"
#define COLOR_BACK ""
#endif

#define RUN_TEST(f) if(!(f())) return TEST_FAILED
#define START_TEST(d) bool test_result = true; std::cout << "Start test: " << __FUNCTION__ << " - " << (d) << std::endl
#define END_TEST end: std::cout << "Done " << __FUNCTION__ << std::string((test_result)? COLOR_GREEN " OK " COLOR_BACK: COLOR_RED " FAILED " COLOR_BACK) << std::endl; return test_result
#define ASSERT_EQUAL(ref, res) if ((res) != (ref)) { std::cout << "Not equal line:" << __LINE__ << std::endl << "Ref:\t" << (ref) << std::endl << "Res:\t" << (res) << std::endl; test_result = false; goto end; } 
#define ASSERT_TRUE(res) if (!(res)) { std::cout << "False at line:" << __LINE__ << std::endl; test_result = false; goto end; } 
#define ASSERT_FALSE(res) if ((res)) { std::cout << "False at line:" << __LINE__ << std::endl; test_result = false; goto end; } 
#define ASSERT_NOT_EQUAL(ref, res) if ((res) == (ref)) { std::cout << "Not equal line:" << __LINE__ << std::endl << "Reference:\t" << (ref) << std::endl << "Result:\t" << (res) << std::endl; test_result = false; goto end; } 
#define FAIL_TEST(reason) { std::cout << "Failed line: " << __LINE__ << " reason: " << (reason) << std::endl; test_result = false; goto end; }