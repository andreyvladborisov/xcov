#include <sstream>
#include <chrono>
#include "clover_test.h"
#include "tests.h"
#include "..\src\x4parser.h"
#include "..\src\clovergenerator.h"

static const std::string  Model[] =
{
    "file:/home/my/projects/hme/Lib/Cpp/Hme/exceptions/TException.cpp",
    "function:24,6,hme::exceptions::TException::ErrorCode() const",
    "function:28,0,hme::exceptions::TException::Message() const",
    "function:32,3,hme::exceptions::TException::TException(hme::exceptions::TErrorCode::type, std::string const&)",
    "function:36,27,hme::exceptions::TException::TException(hme::exceptions::TErrorCode::type)",
    "function:40,0,hme::exceptions::TException::TException()",
    "function:44,0,hme::exceptions::TException::~TException()",
    "function:44,50,hme::exceptions::TException::~TException()",
    "function:47,0,hme::exceptions::TException::operator==(hme::exceptions::TException const&) const",
    "function:53,0,hme::exceptions::TException::operator<(hme::exceptions::TException const&) const",
    "function:61,0,hme::exceptions::TException::operator>(hme::exceptions::TException const&) const",
    "function:69,0,hme::exceptions::TException::toString() const",
    "function:82,0,hme::exceptions::TException::what() const",
    "function:88,0,hme::exceptions::TException::read(hme::transport::TBuffer&, thrift::protocol::TPol*)",
    "function:130,0,hme::exceptions::TException::write(std::shared_ptr<hme::protocol::TState> const&, hme::transport::TBuffer&, hme::protocol::TPol*) const",
    "function:147,0,hme::exceptions::TUserException::TUserException()",
    "function:151,0,hme::exceptions::TUserException::TUserException(std::string const&)",
    "function:155,0,hme::exceptions::TUserException::~TUserException()",
    "function:155,0,hme::exceptions::TUserException::~TUserException()",
    "function:159,0,hme::exceptions::TUserException::operator==(hme::exceptions::TUserException const&) const",

    "lcount:24,6",
    "lcount:26,6",

    "lcount:28,0",
    "lcount:30,0",

    "lcount:32,3",
    "lcount:33,3",
    "branch:33,taken",
    "branch:33,nottaken",
    "lcount:35,3",

    "lcount:36,27",
    "lcount:37,27",
    "branch:37,taken",
    "branch:37,nottaken",
    "lcount:39,27",

    "lcount:40,0",
    "lcount:41,0",
    "branch:41,notexec",
    "branch:41,notexec",
    "lcount:43,0",

    "lcount:44,50",
    "lcount:46,50",
    "branch:46,nottaken",
    "branch:46,taken",

    "lcount:47,0",
    "lcount:49,0",
    "branch:49,notexec",
    "branch:49,notexec",
    "lcount:50,0",
    "branch:50,notexec",
    "branch:50,notexec",
    "lcount:51,0",

    "lcount:53,0",
    "lcount:55,0",
    "branch:55,notexec",
    "branch:55,notexec",
    "lcount:56,0",
    "branch:56,notexec",
    "branch:56,notexec",
    "lcount:57,0",
    "branch:57,notexec",
    "branch:57,notexec",
    "lcount:58,0",

    "lcount:61,0",
    "lcount:63,0",
    "branch:63,notexec",
    "branch:63,notexec",
    "lcount:64,0",
    "branch:64,notexec",
    "branch:64,notexec",
    "lcount:65,0",
    "branch:65,notexec",
    "branch:65,notexec",
    "lcount:66,0",

    "lcount:69,0",
    "lcount:71,0",
    "branch:71,notexec",
    "branch:71,notexec",
    "lcount:72,0",
    "branch:72,notexec",
    "branch:72,notexec",
    "branch:72,notexec",
    "branch:72,notexec",
    "lcount:73,0",
    "branch:73,notexec",
    "branch:73,notexec",
    "lcount:75,0",
    "branch:75,notexec",
    "branch:75,notexec",
    "lcount:76,0",
    "branch:76,notexec",
    "branch:76,notexec",
    "lcount:78,0",
    "branch:78,notexec",
    "branch:78,notexec",
    "lcount:79,0",

    "lcount:82,0",
    "lcount:85,0",
    "branch:85,notexec",
    "branch:85,notexec",
    "branch:85,notexec",
    "branch:85,notexec",

    "lcount:88,0",
    "lcount:90,0",
    "branch:90,notexec",
    "branch:90,notexec",
    "lcount:94,0",
    "branch:94,notexec",
    "branch:94,notexec",
    "lcount:98,0",
    "branch:98,notexec",
    "branch:98,notexec",
    "lcount:99,0",
    "branch:99,notexec",
    "branch:99,notexec",
    "lcount:101,0",
    "lcount:103,0",
    "branch:103,notexec",
    "branch:103,notexec",
    "branch:103,notexec",
    "lcount:106,0",
    "branch:106,notexec",
    "branch:106,notexec",
    "lcount:109,0",
    "branch:109,notexec",
    "branch:109,notexec",
    "lcount:110,0",
    "lcount:112,0",
    "branch:112,notexec",
    "branch:112,notexec",
    "lcount:113,0",
    "lcount:115,0",
    "branch:115,notexec",
    "branch:115,notexec",
    "lcount:117,0",
    "branch:117,notexec",
    "branch:117,notexec",
    "lcount:119,0",
    "branch:119,notexec",
    "branch:119,notexec",
    "lcount:120,0",
    "lcount:122,0",
    "branch:122,notexec",
    "branch:122,notexec",
    "lcount:123,0",
    "lcount:125,0",
    "branch:125,notexec",
    "branch:125,notexec",
    "lcount:127,0",
    "branch:127,notexec",
    "branch:127,notexec",
    "lcount:128,0",

    "lcount:130,0",
    "lcount:133,0",
    "lcount:134,0",
    "lcount:135,0",
    "lcount:136,0",
    "lcount:137,0",
    "branch:137,notexec",
    "branch:137,notexec",
    "lcount:139,0",
    "lcount:140,0",
    "lcount:141,0",
    "lcount:143,0",
    "lcount:144,0",
    "lcount:145,0",

    "lcount:147,0",
    "lcount:149,0",

    "lcount:151,0",
    "lcount:153,0",

    "lcount:155,0",
    "lcount:157,0",
    "branch:157,notexec",
    "branch:157,notexec",

    "lcount:159,0",
    "lcount:161,0"
};

static const std::string Genearted =
{
R"(<?xml version="1.0" encoding="UTF-8"?>
<coverage generated="1320170507" clover="4.1.2">
    <project name="TestProject" timestamp="1320170507">
        <metrics  complexity ="52" elements="80" coveredelements="10" conditionals="75" coveredconditionals="6" statements="28" coveredstatements="3" coveredmethods="4" methods="17" classes="2" loc="80" ncloc="80" files="1" packages="1" />
        <package name="TestProject">
            <metrics  complexity ="52" elements="80" coveredelements="10" conditionals="75" coveredconditionals="6" statements="28" coveredstatements="3" coveredmethods="4" methods="17" classes="2" loc="80" ncloc="80" files="1"/>
            <file name="TException.cpp" path="/home/my/projects/hme/Lib/Cpp/Hme/exceptions/TException.cpp">
                <metrics  complexity ="52" elements="80" coveredelements="10" conditionals="75" coveredconditionals="6" statements="28" coveredstatements="3" coveredmethods="4" methods="17" classes="2" loc="80" ncloc="80"/>
                <class name='hme.exceptions.TException'>
                    <metrics  complexity ="47" elements="72" coveredelements="10" conditionals="73" coveredconditionals="6" statements="25" coveredstatements="3" coveredmethods="4" methods="13"/>
                </class>
                <class name='hme.exceptions.TUserException'>
                    <metrics  complexity ="5" elements="8" coveredelements="0" conditionals="2" coveredconditionals="0" statements="3" coveredstatements="0" coveredmethods="0" methods="4"/>
                </class>
                <line num="24" type="method" complexity="1" count="6" signature='hme::exceptions::TException::ErrorCode() const'/>
                <line num="26" type="stmt" count="6"/>
                <line num="28" type="method" complexity="1" count="0" signature='hme::exceptions::TException::Message() const'/>
                <line num="30" type="stmt" count="0"/>
                <line num="32" type="method" complexity="2" count="3" signature='hme::exceptions::TException::TException(hme::exceptions::TErrorCode::type, std::string const&amp;)'/>
                <line num="33" type="cond" truecount="1" falsecount="1"/>
                <line num="35" type="stmt" count="3"/>
                <line num="36" type="method" complexity="2" count="27" signature='hme::exceptions::TException::TException(hme::exceptions::TErrorCode::type)'/>
                <line num="37" type="cond" truecount="1" falsecount="1"/>
                <line num="39" type="stmt" count="27"/>
                <line num="40" type="method" complexity="2" count="0" signature='hme::exceptions::TException::TException()'/>
                <line num="41" type="cond" truecount="0" falsecount="2"/>
                <line num="43" type="stmt" count="0"/>
                <line num="44" type="method" complexity="2" count="50" signature='hme::exceptions::TException::~TException()'/>
                <line num="46" type="cond" truecount="1" falsecount="1"/>
                <line num="47" type="method" complexity="3" count="0" signature='hme::exceptions::TException::operator==(hme::exceptions::TException const&amp;) const'/>
                <line num="49" type="cond" truecount="0" falsecount="2"/>
                <line num="50" type="cond" truecount="0" falsecount="2"/>
                <line num="51" type="stmt" count="0"/>
                <line num="53" type="method" complexity="4" count="0" signature='hme::exceptions::TException::operator&lt;(hme::exceptions::TException const&amp;) const'/>
                <line num="55" type="cond" truecount="0" falsecount="2"/>
                <line num="56" type="cond" truecount="0" falsecount="2"/>
                <line num="57" type="cond" truecount="0" falsecount="2"/>
                <line num="58" type="stmt" count="0"/>
                <line num="61" type="method" complexity="4" count="0" signature='hme::exceptions::TException::operator&gt;(hme::exceptions::TException const&amp;) const'/>
                <line num="63" type="cond" truecount="0" falsecount="2"/>
                <line num="64" type="cond" truecount="0" falsecount="2"/>
                <line num="65" type="cond" truecount="0" falsecount="2"/>
                <line num="66" type="stmt" count="0"/>
                <line num="69" type="method" complexity="7" count="0" signature='hme::exceptions::TException::toString() const'/>
                <line num="71" type="cond" truecount="0" falsecount="2"/>
                <line num="72" type="cond" truecount="0" falsecount="4"/>
                <line num="73" type="cond" truecount="0" falsecount="2"/>
                <line num="75" type="cond" truecount="0" falsecount="2"/>
                <line num="76" type="cond" truecount="0" falsecount="2"/>
                <line num="78" type="cond" truecount="0" falsecount="2"/>
                <line num="79" type="stmt" count="0"/>
                <line num="82" type="method" complexity="2" count="0" signature='hme::exceptions::TException::what() const'/>
                <line num="85" type="cond" truecount="0" falsecount="4"/>
                <line num="88" type="method" complexity="15" count="0" signature='hme::exceptions::TException::read(hme::transport::TBuffer&amp;, thrift::protocol::TPol*)'/>
                <line num="90" type="cond" truecount="0" falsecount="2"/>
                <line num="94" type="cond" truecount="0" falsecount="2"/>
                <line num="98" type="cond" truecount="0" falsecount="2"/>
                <line num="99" type="cond" truecount="0" falsecount="2"/>
                <line num="101" type="stmt" count="0"/>
                <line num="103" type="cond" truecount="0" falsecount="3"/>
                <line num="106" type="cond" truecount="0" falsecount="2"/>
                <line num="109" type="cond" truecount="0" falsecount="2"/>
                <line num="110" type="stmt" count="0"/>
                <line num="112" type="cond" truecount="0" falsecount="2"/>
                <line num="113" type="stmt" count="0"/>
                <line num="115" type="cond" truecount="0" falsecount="2"/>
                <line num="117" type="cond" truecount="0" falsecount="2"/>
                <line num="119" type="cond" truecount="0" falsecount="2"/>
                <line num="120" type="stmt" count="0"/>
                <line num="122" type="cond" truecount="0" falsecount="2"/>
                <line num="123" type="stmt" count="0"/>
                <line num="125" type="cond" truecount="0" falsecount="2"/>
                <line num="127" type="cond" truecount="0" falsecount="2"/>
                <line num="128" type="stmt" count="0"/>
                <line num="130" type="method" complexity="2" count="0" signature='hme::exceptions::TException::write(std::shared_ptr&lt;hme::protocol::TState&gt; const&amp;, hme::transport::TBuffer&amp;, hme::protocol::TPol*) const'/>
                <line num="133" type="stmt" count="0"/>
                <line num="134" type="stmt" count="0"/>
                <line num="135" type="stmt" count="0"/>
                <line num="136" type="stmt" count="0"/>
                <line num="137" type="cond" truecount="0" falsecount="2"/>
                <line num="139" type="stmt" count="0"/>
                <line num="140" type="stmt" count="0"/>
                <line num="141" type="stmt" count="0"/>
                <line num="143" type="stmt" count="0"/>
                <line num="144" type="stmt" count="0"/>
                <line num="145" type="stmt" count="0"/>
                <line num="147" type="method" complexity="1" count="0" signature='hme::exceptions::TUserException::TUserException()'/>
                <line num="149" type="stmt" count="0"/>
                <line num="151" type="method" complexity="1" count="0" signature='hme::exceptions::TUserException::TUserException(std::string const&amp;)'/>
                <line num="153" type="stmt" count="0"/>
                <line num="155" type="method" complexity="2" count="0" signature='hme::exceptions::TUserException::~TUserException()'/>
                <line num="157" type="cond" truecount="0" falsecount="2"/>
                <line num="159" type="method" complexity="1" count="0" signature='hme::exceptions::TUserException::operator==(hme::exceptions::TUserException const&amp;) const'/>
                <line num="161" type="stmt" count="0"/>
            </file>
        </package>
    </project>
</coverage>)"
};

bool Test_Clover_Statistic()
{
    START_TEST("create model and generate statistic");
    {
        xcov::Project project("TestProject");
        xcov::X4Parser parser(project, "Cpp", "^T\\w+");
        for (const auto& str : Model)
        {
            ASSERT_TRUE(parser.ParseString(str));
        }

        xcov::CloverGenerator cg;
        xcov::CloverStatistic stat;
        cg.GenerateStatitic(project, stat);

        ASSERT_EQUAL(1, stat.package.files);
        ASSERT_EQUAL(2, stat.package.classes);
        ASSERT_EQUAL(17, stat.package.methods);

        xcov::CloverMethodStatistic mstat = stat.methods[project.GetFiles().begin()->second->GetMethod(32)->GetAssociatedIndex()];
        ASSERT_EQUAL(2, mstat.complexity);
        ASSERT_EQUAL(2, mstat.conditionals);
        ASSERT_EQUAL(2, mstat.coveredconditionals);
        ASSERT_EQUAL(3, mstat.coveredelements);
        ASSERT_EQUAL(1, mstat.coveredstatements);
        ASSERT_EQUAL(3, mstat.elements);
        ASSERT_EQUAL(1, mstat.statements);

        mstat = stat.methods[project.GetFiles().begin()->second->GetMethod(82)->GetAssociatedIndex()];
        ASSERT_EQUAL(2, mstat.complexity);
        ASSERT_EQUAL(4, mstat.conditionals);
        ASSERT_EQUAL(0, mstat.coveredconditionals);
        ASSERT_EQUAL(0, mstat.coveredelements);
        ASSERT_EQUAL(0, mstat.coveredstatements);
        ASSERT_EQUAL(2, mstat.elements);
        ASSERT_EQUAL(0, mstat.statements);

        mstat = stat.methods[project.GetFiles().begin()->second->GetMethod(69)->GetAssociatedIndex()];
        ASSERT_EQUAL(7, mstat.complexity);
        ASSERT_EQUAL(14, mstat.conditionals);
        ASSERT_EQUAL(0, mstat.coveredconditionals);
        ASSERT_EQUAL(0, mstat.coveredelements);
        ASSERT_EQUAL(0, mstat.coveredstatements);
        ASSERT_EQUAL(8, mstat.elements);
        ASSERT_EQUAL(1, mstat.statements);

        ASSERT_EQUAL(80, stat.package.loc);
        ASSERT_EQUAL(80, stat.package.ncloc);

    }
    END_TEST;
}

bool Test_Clover_Generator()
{
    START_TEST("create model and generate it");
    {
        xcov::Project project("TestProject");
        xcov::X4Parser parser(project, "Cpp", "^T\\w+");
        for (const auto& str : Model)
        {
            ASSERT_TRUE(parser.ParseString(str));
        }

        std::ostringstream ostr;
        xcov::CloverGenerator cg;
        std::chrono::seconds timestamp { 1320170507 };
                   
        cg.Generate(project, ostr, timestamp);
        std::string output = ostr.str();
        ASSERT_EQUAL(Genearted, ostr.str());
    }
    END_TEST;
}
