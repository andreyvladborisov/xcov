#pragma once

#include "../src/model.h"


bool Test_Model_Project();
bool Test_Model_Class();
bool Test_Model_Method();
bool Test_Model_Branch();
bool Test_Model_Line();
bool Test_Model_File();