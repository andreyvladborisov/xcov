#include "x4parser_test.h"
#include "tests.h"

struct TestMethodsByNumber
{
public:
    std::string name;
    int start;
    int stop;
    int executed;
    std::string class_name;
    int lines;
    int branches;
};

const TestMethodsByNumber  MTDS_BY_ID [] =
{
    {"hme::exceptions::TException::ErrorCode() const", 24, 26, 6, "hme::exceptions::TException", 2, 0},
    {"hme::exceptions::TException::Message() const", 28, 30, 0, "hme::exceptions::TException", 2, 0},
    {"hme::exceptions::TException::TException(hme::exceptions::TErrorCode::type, std::string const&)", 32, 35, 3, "hme::exceptions::TException", 3, 1},
    {"hme::exceptions::TException::TException(hme::exceptions::TErrorCode::type)", 36, 39, 27, "hme::exceptions::TException", 3, 1},
    {"hme::exceptions::TException::TException()", 40, 43, 0, "hme::exceptions::TException", 3, 1},
    {"hme::exceptions::TException::~TException()", 44, 46, 50, "hme::exceptions::TException", 2, 1},
    {"hme::exceptions::TException::operator==(hme::exceptions::TException const&) const", 47, 51, 0, "hme::exceptions::TException", 4, 2},
    {"hme::exceptions::TException::operator<(hme::exceptions::TException const&) const", 53, 58, 0, "hme::exceptions::TException", 5, 3},
    {"hme::exceptions::TException::operator>(hme::exceptions::TException const&) const", 61, 66, 0, "hme::exceptions::TException", 5, 3},
    {"hme::exceptions::TException::toString() const", 69, 79, 0, "hme::exceptions::TException", 8, 6},
    {"hme::exceptions::TException::what() const", 82, 85, 0, "hme::exceptions::TException", 2, 1},
    {"hme::exceptions::TException::read(hme::transport::TBuffer&, thrift::protocol::TPol*)", 88, 128, 0, "hme::exceptions::TException", 21, 14},
    {"hme::exceptions::TException::write(std::shared_ptr<hme::protocol::TState> const&, hme::transport::TBuffer&, hme::protocol::TPol*) const", 130, 145, 0, "hme::exceptions::TException", 12, 1},
    {"hme::exceptions::TUserException::TUserException()", 147, 149, 0, "hme::exceptions::TUserException", 2, 0},
    {"hme::exceptions::TUserException::TUserException(std::string const&)", 151, 153, 0, "hme::exceptions::TUserException", 2, 0},
    {"hme::exceptions::TUserException::~TUserException()", 155, 157, 0, "hme::exceptions::TUserException", 2, 1},
    {"hme::exceptions::TUserException::operator==(hme::exceptions::TUserException const&) const", 159, 161, 0, "hme::exceptions::TUserException", 2, 0}
};

bool Test_X4Parser_Happy()
{
    START_TEST("parse gcov file");
    {
        xcov::Project project("TestProject");
        xcov::X4Parser parser(project, "Hme", "^T\\w+");
        ASSERT_TRUE(parser.ParseString("file:/home/my/projects/hme/Lib/Cpp/Hme/exceptions/TException.cpp"));
        ASSERT_TRUE(parser.ParseString("function:24,6,hme::exceptions::TException::ErrorCode() const"));
        ASSERT_TRUE(parser.ParseString("function:28,0,hme::exceptions::TException::Message() const"));
        ASSERT_TRUE(parser.ParseString("function:32,3,hme::exceptions::TException::TException(hme::exceptions::TErrorCode::type, std::string const&)"));
        ASSERT_TRUE(parser.ParseString("function:36,27,hme::exceptions::TException::TException(hme::exceptions::TErrorCode::type)"));
        ASSERT_TRUE(parser.ParseString("function:40,0,hme::exceptions::TException::TException()"));
        ASSERT_TRUE(parser.ParseString("function:44,0,hme::exceptions::TException::~TException()"));
        ASSERT_TRUE(parser.ParseString("function:44,50,hme::exceptions::TException::~TException()"));
        ASSERT_TRUE(parser.ParseString("function:47,0,hme::exceptions::TException::operator==(hme::exceptions::TException const&) const"));
        ASSERT_TRUE(parser.ParseString("function:53,0,hme::exceptions::TException::operator<(hme::exceptions::TException const&) const"));
        ASSERT_TRUE(parser.ParseString("function:61,0,hme::exceptions::TException::operator>(hme::exceptions::TException const&) const"));
        ASSERT_TRUE(parser.ParseString("function:69,0,hme::exceptions::TException::toString() const"));
        ASSERT_TRUE(parser.ParseString("function:82,0,hme::exceptions::TException::what() const"));
        ASSERT_TRUE(parser.ParseString("function:88,0,hme::exceptions::TException::read(hme::transport::TBuffer&, thrift::protocol::TPol*)"));
        ASSERT_TRUE(parser.ParseString("function:130,0,hme::exceptions::TException::write(std::shared_ptr<hme::protocol::TState> const&, hme::transport::TBuffer&, hme::protocol::TPol*) const"));
        ASSERT_TRUE(parser.ParseString("function:147,0,hme::exceptions::TUserException::TUserException()"));
        ASSERT_TRUE(parser.ParseString("function:151,0,hme::exceptions::TUserException::TUserException(std::string const&)"));
        ASSERT_TRUE(parser.ParseString("function:155,0,hme::exceptions::TUserException::~TUserException()"));
        ASSERT_TRUE(parser.ParseString("function:155,0,hme::exceptions::TUserException::~TUserException()"));
        ASSERT_TRUE(parser.ParseString("function:159,0,hme::exceptions::TUserException::operator==(hme::exceptions::TUserException const&) const"));

        ASSERT_TRUE(parser.ParseString("lcount:24,6"));
        ASSERT_TRUE(parser.ParseString("lcount:26,6"));

        ASSERT_TRUE(parser.ParseString("lcount:28,0"));
        ASSERT_TRUE(parser.ParseString("lcount:30,0"));
        
        ASSERT_TRUE(parser.ParseString("lcount:32,3"));
        ASSERT_TRUE(parser.ParseString("lcount:33,3"));
        ASSERT_TRUE(parser.ParseString("branch:33,taken"));
        ASSERT_TRUE(parser.ParseString("branch:33,nottaken"));
        ASSERT_TRUE(parser.ParseString("lcount:35,3"));
        
        ASSERT_TRUE(parser.ParseString("lcount:36,27"));
        ASSERT_TRUE(parser.ParseString("lcount:37,27"));
        ASSERT_TRUE(parser.ParseString("branch:37,taken"));
        ASSERT_TRUE(parser.ParseString("branch:37,nottaken"));
        ASSERT_TRUE(parser.ParseString("lcount:39,27"));
        
        ASSERT_TRUE(parser.ParseString("lcount:40,0"));
        ASSERT_TRUE(parser.ParseString("lcount:41,0"));
        ASSERT_TRUE(parser.ParseString("branch:41,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:41,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:43,0"));
        
        ASSERT_TRUE(parser.ParseString("lcount:44,50"));
        ASSERT_TRUE(parser.ParseString("lcount:46,50"));
        ASSERT_TRUE(parser.ParseString("branch:46,nottaken"));
        ASSERT_TRUE(parser.ParseString("branch:46,taken"));
        
        ASSERT_TRUE(parser.ParseString("lcount:47,0"));
        ASSERT_TRUE(parser.ParseString("lcount:49,0"));
        ASSERT_TRUE(parser.ParseString("branch:49,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:49,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:50,0"));
        ASSERT_TRUE(parser.ParseString("branch:50,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:50,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:51,0"));
        
        ASSERT_TRUE(parser.ParseString("lcount:53,0"));
        ASSERT_TRUE(parser.ParseString("lcount:55,0"));
        ASSERT_TRUE(parser.ParseString("branch:55,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:55,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:56,0"));
        ASSERT_TRUE(parser.ParseString("branch:56,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:56,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:57,0"));
        ASSERT_TRUE(parser.ParseString("branch:57,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:57,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:58,0"));
        
        ASSERT_TRUE(parser.ParseString("lcount:61,0"));
        ASSERT_TRUE(parser.ParseString("lcount:63,0"));
        ASSERT_TRUE(parser.ParseString("branch:63,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:63,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:64,0"));
        ASSERT_TRUE(parser.ParseString("branch:64,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:64,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:65,0"));
        ASSERT_TRUE(parser.ParseString("branch:65,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:65,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:66,0"));
        
        ASSERT_TRUE(parser.ParseString("lcount:69,0"));
        ASSERT_TRUE(parser.ParseString("lcount:71,0"));
        ASSERT_TRUE(parser.ParseString("branch:71,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:71,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:72,0"));
        ASSERT_TRUE(parser.ParseString("branch:72,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:72,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:72,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:72,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:73,0"));
        ASSERT_TRUE(parser.ParseString("branch:73,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:73,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:75,0"));
        ASSERT_TRUE(parser.ParseString("branch:75,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:75,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:76,0"));
        ASSERT_TRUE(parser.ParseString("branch:76,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:76,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:78,0"));
        ASSERT_TRUE(parser.ParseString("branch:78,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:78,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:79,0"));
        
        ASSERT_TRUE(parser.ParseString("lcount:82,0"));
        ASSERT_TRUE(parser.ParseString("lcount:85,0"));
        ASSERT_TRUE(parser.ParseString("branch:85,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:85,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:85,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:85,notexec"));
        
        ASSERT_TRUE(parser.ParseString("lcount:88,0"));
        ASSERT_TRUE(parser.ParseString("lcount:90,0"));
        ASSERT_TRUE(parser.ParseString("branch:90,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:90,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:94,0"));
        ASSERT_TRUE(parser.ParseString("branch:94,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:94,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:98,0"));
        ASSERT_TRUE(parser.ParseString("branch:98,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:98,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:99,0"));
        ASSERT_TRUE(parser.ParseString("branch:99,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:99,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:101,0"));
        ASSERT_TRUE(parser.ParseString("lcount:103,0"));
        ASSERT_TRUE(parser.ParseString("branch:103,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:103,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:103,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:106,0"));
        ASSERT_TRUE(parser.ParseString("branch:106,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:106,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:109,0"));
        ASSERT_TRUE(parser.ParseString("branch:109,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:109,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:110,0"));
        ASSERT_TRUE(parser.ParseString("lcount:112,0"));
        ASSERT_TRUE(parser.ParseString("branch:112,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:112,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:113,0"));
        ASSERT_TRUE(parser.ParseString("lcount:115,0"));
        ASSERT_TRUE(parser.ParseString("branch:115,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:115,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:117,0"));
        ASSERT_TRUE(parser.ParseString("branch:117,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:117,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:119,0"));
        ASSERT_TRUE(parser.ParseString("branch:119,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:119,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:120,0"));
        ASSERT_TRUE(parser.ParseString("lcount:122,0"));
        ASSERT_TRUE(parser.ParseString("branch:122,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:122,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:123,0"));
        ASSERT_TRUE(parser.ParseString("lcount:125,0"));
        ASSERT_TRUE(parser.ParseString("branch:125,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:125,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:127,0"));
        ASSERT_TRUE(parser.ParseString("branch:127,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:127,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:128,0"));
        
        ASSERT_TRUE(parser.ParseString("lcount:130,0"));
        ASSERT_TRUE(parser.ParseString("lcount:133,0"));
        ASSERT_TRUE(parser.ParseString("lcount:134,0"));
        ASSERT_TRUE(parser.ParseString("lcount:135,0"));
        ASSERT_TRUE(parser.ParseString("lcount:136,0"));
        ASSERT_TRUE(parser.ParseString("lcount:137,0"));
        ASSERT_TRUE(parser.ParseString("branch:137,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:137,notexec"));
        ASSERT_TRUE(parser.ParseString("lcount:139,0"));
        ASSERT_TRUE(parser.ParseString("lcount:140,0"));
        ASSERT_TRUE(parser.ParseString("lcount:141,0"));
        ASSERT_TRUE(parser.ParseString("lcount:143,0"));
        ASSERT_TRUE(parser.ParseString("lcount:144,0"));
        ASSERT_TRUE(parser.ParseString("lcount:145,0"));
        
        ASSERT_TRUE(parser.ParseString("lcount:147,0"));
        ASSERT_TRUE(parser.ParseString("lcount:149,0"));
        
        ASSERT_TRUE(parser.ParseString("lcount:151,0"));
        ASSERT_TRUE(parser.ParseString("lcount:153,0"));
        
        ASSERT_TRUE(parser.ParseString("lcount:155,0"));
        ASSERT_TRUE(parser.ParseString("lcount:157,0"));
        ASSERT_TRUE(parser.ParseString("branch:157,notexec"));
        ASSERT_TRUE(parser.ParseString("branch:157,notexec"));
        
        ASSERT_TRUE(parser.ParseString("lcount:159,0"));
        ASSERT_TRUE(parser.ParseString("lcount:161,0"));

        ASSERT_EQUAL(1, project.GetFiles().size());
        auto fit = project.GetFiles().cbegin();
        ASSERT_EQUAL("/home/my/projects/hme/Lib/Cpp/Hme/exceptions/TException.cpp", fit->second->GetName());
        ASSERT_EQUAL(17, fit->second->GetMethods().size());

        int i = 0;
        for (auto mt : fit->second->GetMethods())
        {
            ASSERT_EQUAL(MTDS_BY_ID[i].name, mt.second->GetName());
            ASSERT_EQUAL(MTDS_BY_ID[i].start, mt.second->GetStartLine());
            ASSERT_EQUAL(MTDS_BY_ID[i].stop, mt.second->GetEndLine());
            ASSERT_EQUAL(MTDS_BY_ID[i].executed, mt.second->GetExecuted());
            ASSERT_EQUAL(MTDS_BY_ID[i].class_name, mt.second->GetClass().lock()->GetName());
            xcov::MethodPtr mc = mt.second->GetClass().lock()->GetMethod(mt.second->GetName());
            ASSERT_EQUAL(mt.second, mc);
            ASSERT_EQUAL(MTDS_BY_ID[i].lines, mt.second->GetLines().size());
            ASSERT_EQUAL(MTDS_BY_ID[i].branches, mt.second->GetBranches().size());
            i++;
        }

        xcov::MethodPtr m82 = fit->second->GetMethod(82);
        ASSERT_TRUE(m82);
        xcov::BranchPtr br1 = m82->GetBranch(85);
        ASSERT_TRUE(br1);
        ASSERT_EQUAL(4, br1->GetVariants().size());
        xcov::BranchPtr br2 = m82->GetBranch(82);
        ASSERT_FALSE(br2);

        xcov::MethodPtr m32 = fit->second->GetMethod(32);
        xcov::BranchPtr br3 = m32->GetBranch(33);
        ASSERT_EQUAL(2, br3->GetVariants().size());
        ASSERT_EQUAL(xcov::Taken, br3->GetVariants()[0]);
        ASSERT_EQUAL(xcov::NotTaken, br3->GetVariants()[1]);

    }
    END_TEST;
}

bool Test_X4Parser_Error()
{
    START_TEST("parse gcov file");
    {
        xcov::Project project("TestProject");
        xcov::X4Parser parser(project, "Hme", "^T\\w+");
        ASSERT_FALSE(parser.ParseString("fil:/home/my/projects/hme/Lib/Cpp/Hme/exceptions/TException.cpp"));
        ASSERT_FALSE(parser.ParseString("file :/home/my/projects/hme/Lib/Cpp/Hme/exceptions/TException.cpp"));
        ASSERT_FALSE(parser.ParseString("xfile:/home/my/projects/hme/Lib/Cpp/Hme/exceptions/TException.cpp"));
        ASSERT_TRUE(parser.ParseString("file:/home/my/projects/hme/Lib/Cpp/Hme/exceptions/TException.cpp"));
        ASSERT_FALSE(parser.ParseString("functio:24,6,hme::exceptions::TException::ErrorCode() const"));
        ASSERT_FALSE(parser.ParseString("xfunction:24,6,hme::exceptions::TException::ErrorCode() const"));
        ASSERT_FALSE(parser.ParseString("function:6,hme::exceptions::TException::ErrorCode() const"));
        ASSERT_FALSE(parser.ParseString("function:m,6,hme::exceptions::TException::ErrorCode() const"));
        ASSERT_FALSE(parser.ParseString("lcoun:24,6"));
        ASSERT_FALSE(parser.ParseString("xlcount:24,6"));
        ASSERT_FALSE(parser.ParseString("xlcount:d,6"));
        ASSERT_FALSE(parser.ParseString("branc:33,taken"));
        ASSERT_FALSE(parser.ParseString("xbranch:33,taken"));
        ASSERT_FALSE(parser.ParseString("xbranch:d,taken"));
    }
    END_TEST;

    return false;
}
