all: xcov

xcov: model.o fname.o x4parser.o clovergenerator.o mkxcov.o
	$(CXX) -o xcov model.o fname.o x4parser.o clovergenerator.o mkxcov.o	

model.o: src/model.cpp src/model.h
	$(CXX) -c -std=c++11 -Isrc src/model.cpp

fname.o: src/fname.cpp src/fname.h
	$(CXX) -c -std=c++11 -Isrc src/fname.cpp

x4parser.o: src/x4parser.cpp src/x4parser.h
	$(CXX) -c -std=c++11 -Isrc src/x4parser.cpp

clovergenerator.o: src/clovergenerator.cpp src/clovergenerator.h src/igenerator.h
	$(CXX) -c -std=c++11 -Isrc src/clovergenerator.cpp

mkxcov.o: mkxcov.cpp
	$(CXX) -c -std=c++11 -Isrc mkxcov.cpp

clean:
	rm -f model.o fname.o x4parser.o clovergenerator.o mkxcov.o
