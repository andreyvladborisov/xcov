#include <iostream>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <sstream>
#include <regex>
#include <fstream>
#include <chrono>
#include "x4parser.h"
#include "clovergenerator.h"

const std::string gHelpString
{
R"(use xcov v1.0 in the following way:
./xcov directory name [options]
Possible options:

--project-name "name"           The name of the project.
                                Default: "" 

--build-directory path          The path to the folder where the tool will create temporary folder with files. 
                                Default: folder gcov.XXXXXX will be created in current location. 
                                The folder will be removed after program finished.
--object-directory path         The path to the folder with *.gcda files
                                Default: directory name

--output-file path              Path to the output file. 
                                Default: the clover.xml file will be created at the current folder.

--gcov-toolset path             Path to gcov utility in case not a system installed utility is required.
                                Default: "gcov"

--verbose-mode true|false        Provide additional output.
                                Default: false

--path-to-include "regex"       The regex expression for select files that participate in report. 
                                Example: --path-to-include="/Lib/Cpp" - takes all files that include that subpath.
                                Default: .*

--class-name-pattern "regex"    The regex expression to differentiate the class name from namespace name. 
                                It is applied to the first lexem separated from function name with "::".
                                Example: --class-name-pattern="^T\w*" for the class name like TException.
                                Default: "^[A-Z]\w+".
)"
};

struct Arguments
{
    
    const std::string opt_project_name{ "--project-name" };
    const std::string opt_build_directory{ "--build-directory" };
    const std::string opt_object_directory{ "--object-directory" };
    const std::string opt_output_file{ "--output-file" };
    const std::string opt_gcov_toolset{ "--gcov-toolset" };
    const std::string opt_silent_mode{ "--verbose-mode" };
    const std::string opt_path_to_include{ "--path-to-include" };
    const std::string opt_class_name_pattern{ "--class-name-pattern" };
public:
    std::string project_name;
    std::string directory;
    std::string object_directory;
    std::string build_directory;
    std::string output_file;
    std::string gcov;
    bool        verbose_mode;
    std::string include_pattern;
    std::string class_pattern;
    Arguments()
        : project_name("")
        , directory(".")
        , build_directory(".")
        , object_directory(".")
        , output_file("clover.xml")
        , gcov("gcov")
        , verbose_mode(false)
        , include_pattern(".*")
        , class_pattern(R"(^[A-Z]\w+)")
    {
    }

    bool Parse(int argc, char *argv [])
    {
        bool res = false;
        if (argc > 2)
        {
            res = true;
            directory = argv[1];
            object_directory = directory;
            for (int i = 2; i < argc; i++)
            {
                if (argv[i] == opt_project_name)
                {
                    if (++i < argc)
                    {
                        project_name = argv[i];
                    }
                    else
                    {
                        res = false;
                        break;
                    }
                }
                else if (argv[i] == opt_build_directory)
                {
                    if (++i < argc)
                    {
                        build_directory = argv[i];
                        output_file = build_directory + "/clover.xml";
                    }
                    else
                    {
                        res = false;
                        break;
                    }
                }
                else if (argv[i] == opt_object_directory)
                {
                    if (++i < argc)
                    {
                        object_directory = argv[i];
                    }
                    else
                    {
                        res = false;
                        break;
                    }
                }
                else if (argv[i] == opt_output_file)
                {
                    if (++i < argc)
                    {
                        output_file = argv[i];
                    }
                    else
                    {
                        res = false;
                        break;
                    }
                }
                else if (argv[i] == opt_gcov_toolset)
                {
                    if (++i < argc)
                    {
                        gcov = argv[i];
                    }
                    else
                    {
                        res = false;
                        break;
                    }
                }
                else if (argv[i] == opt_silent_mode)
                {
                    if (++i < argc)
                    {
                        verbose_mode = (argv[i] == std::string("true"));
                    }
                    else
                    {
                        res = false;
                        break;
                    }
                }
                else if (argv[i] == opt_path_to_include)
                {
                    if (++i < argc)
                    {
                        include_pattern = argv[i];
                    }
                    else
                    {
                        res = false;
                        break;
                    }
                }
                else if (argv[i] == opt_class_name_pattern)
                {
                    if (++i < argc)
                    {
                        class_pattern = argv[i];
                    }
                    else
                    {
                        res = false;
                        break;
                    }
                }
                else
                {
                    res = false;
                    break;
                }
            }
        }
        return res;
    }
};


std::string exec(const std::string& cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::shared_ptr<FILE> pipe(popen(cmd.c_str(), "r"), pclose);
    if (pipe)
    {
        while (!feof(pipe.get()))
        {
            if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
            {
                result += buffer.data();
            }
        }
    }
    return result;
}

int main(int argc, char *argv [])
{
    Arguments args;
    if (!args.Parse(argc, argv))
    {
        std::cout << gHelpString << std::endl;
        return -1;
    }

    //Create temporary folder
    exec(std::string("mkdir -p ") + args.build_directory);
    std::istringstream gcov_folder{ exec(std::string("mktemp ") + args.build_directory + "/" + "gcov.XXXXXXX" + " -d") };
    std::string gcov_folder_name;
    if (!std::getline(gcov_folder, gcov_folder_name))
    {
        std::cerr << "Can't create tmp folder for gcov files" << std::endl;
        return -1;
    }

    std::istringstream files(exec(std::string() + "find " + args.directory + " -type f -name '*.cpp'"));
    std::regex rgx_path(args.include_pattern);
    std::string file;

    while (std::getline(files, file))
    {
        if (std::regex_search(file, rgx_path))
        {

            std::istringstream basename{ exec(std::string() + "basename " + file) };
            std::string filename_ext;
            std::string filename;
            std::getline(basename, filename_ext);
            filename = filename_ext.substr(0, filename_ext.length() - 4);
            std::string find_cmd {std::string("") + "find " + args.object_directory + " -type f -name '" + filename + ".gcno" + "' | sed -r 's|/[^/]+$||' |sort |uniq"};
            std::istringstream object_folder(exec(find_cmd));
            std::string object_folder_name = "";
            std::getline(object_folder, object_folder_name);
            
            if(object_folder_name.empty())
            {
                find_cmd  = std::string("") + "find " + args.object_directory + " -type f -name '" + filename_ext + ".gcno" + "'";
                std::istringstream alt_file {exec(find_cmd)};                                        
                std::string alt_file_name = "";
                std::getline(alt_file, alt_file_name);
                if(!alt_file_name.empty())
                {
                    std::string copy_cmd { std::string("cp ") + alt_file_name + " " + gcov_folder_name + "/" +  filename + ".gcno"};
                    exec(copy_cmd);
                }

                find_cmd  = std::string("") + "find " + args.object_directory + " -type f -name '" + filename_ext + ".gcda" + "'";
                std::istringstream alt_file2 {exec(find_cmd)};                                        
                std::string alt_file_name2 = "";
                std::getline(alt_file2, alt_file_name2);
                if(!alt_file_name2.empty())
                {
                    std::string copy_cmd { std::string("cp ") + alt_file_name2 + " " + gcov_folder_name + "/" +  filename + ".gcda"};
                    exec(copy_cmd);
                }

                object_folder_name = gcov_folder_name;
            }


            if (!object_folder_name.empty())
            {
                std::string cmd_str =std::string("(cd ") + gcov_folder_name + " && " + args.gcov + " " + file + " -i -m -b -c -p " + "--object-directory " + object_folder_name + " )";
                if (args.verbose_mode)
                {
                    std::cout << cmd_str << std::endl;
                }
                std::string report = exec(cmd_str);
                (void) report;
            }
        }
    }


    xcov::Project proj(args.project_name);
    xcov::X4Parser parser(proj, args.include_pattern, args.class_pattern);

    //For each gcov file run parser
    std::istringstream gcovs{ exec(std::string() + "find " + gcov_folder_name + " -type f -name '*.gcov'") };
    std::string gcov;
    while (std::getline(gcovs, gcov))
    {
        if (args.verbose_mode)
        {
            std::cout << "Parse " << gcov << std::endl;
        }
        std::ifstream infile(gcov);
        std::string line;
        int counter = 0;
        while (std::getline(infile, line))
        {
            counter++;
            if (!parser.ParseString(line))
            {
                std::cerr << "Error parsing " << gcov << " at line " << counter << ":"<< line << std::endl;
            }
            
        }
    }

    xcov::CloverGenerator genrator;
    auto s = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());
    std::ofstream report(args.output_file);
    if (args.verbose_mode)
    {
        std::cout << "xcov v1.0: generate report " << gcov << std::endl;
    }
    genrator.Generate(proj, report, s);
    report.flush();

    //Remove temporary folder
    exec(std::string("rm -rf ") + gcov_folder_name);
    return 0;
}